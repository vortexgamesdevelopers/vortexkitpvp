package nl.daan.commands;

import nl.daan.KitPvP;
import nl.daan.data.Data;
import nl.daan.data.objects.player.DataPlayer;
import nl.daan.types.Rank;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Collections;
import java.util.List;

import static nl.daan.KitPvP.color;

public class RankCommand extends CommandBase {

    public RankCommand(){
        super("rank", "op");
    }

    @Override
    public void executeCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) return;
        Player player = (Player) sender;
        if (args.length == 0){
            player.sendMessage(color("&aMogelijke arguments:"));
            player.sendMessage(color("&a - set"));
            player.sendMessage(color("&a - list"));
        } else {
            if (args[0].equalsIgnoreCase("set")){
                if (args.length != 2){
                    OfflinePlayer target = Bukkit.getOfflinePlayer(args[1]);
                    DataPlayer dataPlayer = Data.getPlayer(target);
                    Rank oldRank = dataPlayer.getRank();
                    if (!rankExists(args[2])){
                        player.sendMessage(color("&cRank niet gevonden."));
                    } else {
                        Rank rank = Rank.valueOf(args[2].toUpperCase());
                        dataPlayer.setRank(rank);
                        if (target.isOnline()){
                            KitPvP.getScoreboards().removePlayer(target.getPlayer(), oldRank);
                            KitPvP.getScoreboards().addPlayer(target.getPlayer(), rank);
                        }
                        player.sendMessage(color("&aRank geupdate naar \"" + rank.toString() + "\""));
                    }
                }
            } else if (args[0].equalsIgnoreCase("list")){
                for (Rank rank : Rank.values()){
                    player.sendMessage(color(rank.getPrefix()));
                }
            } else {
                player.sendMessage(color("&cArgument niet gevonden."));
            }
        }
    }

    @Override
    public List<String> tabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return Collections.emptyList();
    }

    private boolean rankExists(String rank){
        for (Rank name : Rank.values()){
            if (name.toString().equalsIgnoreCase(rank)){
                return true;
            }
        }
        return false;
    }

}
