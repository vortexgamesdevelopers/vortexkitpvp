package nl.daan.commands;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

import static nl.daan.KitPvP.color;

public class UnbreakableCommand extends CommandBase {

    public UnbreakableCommand(){
        super("unbreakable", "op");
    }

    @Override
    public void executeCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) return;
        Player player = (Player) sender;
        if (player.getItemInHand().getType().equals(Material.AIR)){
            player.sendMessage(color("&cKan niet het item in je hand unbreakable maken!"));
        } else {
            if (!player.getItemInHand().getItemMeta().spigot().isUnbreakable()){
                ItemStack itemStack = player.getItemInHand();
                ItemMeta itemMeta = itemStack.getItemMeta();
                itemMeta.spigot().setUnbreakable(true);
                itemMeta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                itemStack.setItemMeta(itemMeta);
                player.setItemInHand(itemStack);
            } else {
                ItemStack itemStack = player.getItemInHand();
                ItemMeta itemMeta = itemStack.getItemMeta();
                itemMeta.spigot().setUnbreakable(false);
                itemMeta.removeItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                itemStack.setItemMeta(itemMeta);
                player.setItemInHand(itemStack);
            }
        }
    }

    @Override
    public List<String> tabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return null;
    }

}
