package nl.daan.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;

import java.util.Collections;
import java.util.List;

public class SpawnCommand extends CommandBase implements Listener {

    private int taskId;

    public SpawnCommand(){
        super("spawn");
    }

    @Override
    public void executeCommand(CommandSender sender, Command cmd, String label, String[] args) {

    }

    @Override
    public List<String> tabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return Collections.emptyList();
    }
}
