package nl.daan.commands;

import nl.daan.KitPvP;
import nl.daan.api.events.KitChangePermissionEvent;
import nl.daan.api.events.KitCreateEvent;
import nl.daan.api.events.KitEditEvent;
import nl.daan.data.Data;
import nl.daan.data.objects.kit.Kit;
import nl.daan.data.objects.kit.KitItem;
import nl.daan.managers.LeaderboardManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Collections;
import java.util.List;

import static nl.daan.KitPvP.color;

public class KitCommand extends CommandBase {

    public KitCommand(){
        super("kit", "op");
    }

    @Override
    public void executeCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) return;
        Player player = (Player) sender;
        if (args.length == 0){
            player.sendMessage(color("&aMogelijke arguments:\n - create\n - edit\n - setpermission\n - setdisplayname\n - list\n - setguiitem"));
        } else if (args.length > 0) {
            if (args[0].equalsIgnoreCase("create")) {
                if (args.length < 2) {
                    player.sendMessage(color("&cJe moet wel een kit naam invullen."));
                } else {
                    StringBuilder stringBuilder = new StringBuilder();
                    for (int i = 1; i < args.length; i++) stringBuilder.append(args[i]);
                    KitCreateEvent event = new KitCreateEvent(player, stringBuilder.toString().replaceAll(" ", ""));
                    Bukkit.getPluginManager().callEvent(event);
                    if (!event.isCancelled()){
                        event.execute();
                    }
                }
            } else if (args[0].equalsIgnoreCase("edit")) {
                if (args.length != 2) {
                    player.sendMessage(color("&cJe moet een kit naam invullen."));
                } else {
                    String kitName = args[1];
                    KitEditEvent event = new KitEditEvent(player, kitName);
                    Bukkit.getPluginManager().callEvent(event);
                    if (!event.isCancelled()){
                        event.execute();
                    }
                }
            } else if (args[0].equalsIgnoreCase("setpermission")) {
                if (args.length != 3) {
                    player.sendMessage(color("&cEr moet wel een permission in gevuld worden."));
                } else {
                    String kitName = args[1];
                    KitChangePermissionEvent event = new KitChangePermissionEvent(player, kitName, args[2]);
                    Bukkit.getPluginManager().callEvent(event);
                    if (!event.isCancelled()){
                        event.execute();
                    }
                }
            } else if (args[0].equalsIgnoreCase("setdisplayname")) {
                if (args.length < 1) {
                    player.sendMessage(color("&cEr moet wel een naam gegeven worden."));
                } else {
                    String kitName = args[1];
                    Kit kit = Data.getKit(kitName);
                    StringBuilder stringBuilder = new StringBuilder();
                    for (int i = 2; i < args.length; i++) {
                        stringBuilder.append(color(args[i]));
                    }
                    kit.setDisplayName(stringBuilder.toString());
                    player.sendMessage(color("&aDisplayname voor de kit \"" + kitName + "\" aangepast naar \"" + stringBuilder.toString() + "\""));
                }
            } else if (args[0].equalsIgnoreCase("list")) {
                if (KitPvP.DEV) player.sendMessage(color("&aTotal Kits: " + Data.kits.size()));
                if (KitPvP.DEV) Data.kits.stream().forEach(kit -> player.sendMessage(kit.toString()));
                player.sendMessage(color("&aMogelijke kits:"));
                Data.kits.stream().forEach(kit -> {
                    player.sendMessage(color(" &a- " + kit.getDisplayName() + "&a | " + kit.getPermission()));
                });
            } else if (args[0].equalsIgnoreCase("setguiitem")) {
                if (args.length < 2) {
                    player.sendMessage(color("&cJe moet wel een kit naam geven."));
                } else {
                    if (player.getItemInHand() == null) {
                        player.sendMessage(color("&cJe hebt geen item in je hand."));
                    } else {
                        String kitName = args[1];
                        Kit kit = Data.getKit(kitName);
                        kit.setGuiItem(new KitItem(player.getItemInHand()));
                        player.sendMessage(color("&aJe hebt het gui item van de kit \"" + kitName + "\" aangepast."));
                    }
                }
            } else if (args[0].equalsIgnoreCase("fixnopvp")) {
                player.getWorld().getBlockAt(player.getLocation().getBlockX(), 0, player.getLocation().getBlockZ()).setType(Material.DIAMOND_BLOCK);
                player.sendMessage("Block gezet");
            } else if (args[0].equalsIgnoreCase("leaderboardkills")){
                LeaderboardManager.sendTopKills(player);
            } else {
                player.sendMessage(color("&cArgument niet gevonden."));
            }
        }
    }

    @Override
    public List<String> tabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return null;
    }
}
