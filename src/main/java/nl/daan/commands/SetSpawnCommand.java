package nl.daan.commands;

import nl.daan.KitPvP;
import nl.daan.utils.LocationSerializer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SetSpawnCommand extends CommandBase {

    public SetSpawnCommand(){
        super("setspawn", "op");
    }

    @Override
    public void executeCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) return;
        Player player = (Player) sender;
        if (args.length == 0){
            KitPvP.getInstance().getConfig().set("spawn", LocationSerializer.serialize(player.getLocation()));
            KitPvP.getInstance().saveConfig();
            player.sendMessage("spawn verzet");
        } else {
            if (args[0].equalsIgnoreCase("sumo")){
                KitPvP.getInstance().getConfig().set("sumo", LocationSerializer.serialize(player.getLocation()));
                KitPvP.getInstance().saveConfig();
                player.sendMessage("Sumo spawn verzet");
            }
            if (args[0].equalsIgnoreCase("kiteditor")){
                KitPvP.getInstance().getConfig().set("kitEditor", LocationSerializer.serialize(player.getLocation()));
                KitPvP.getInstance().saveConfig();
                player.sendMessage("kiteditor spawn verzet");
            }
        }

    }

    @Override
    public List<String> tabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        List<String> returnList = new ArrayList<>();
        if (args.length == 1){
            return Arrays.asList("sumo", "kiteditor");
        }
        return Collections.emptyList();
    }
}
