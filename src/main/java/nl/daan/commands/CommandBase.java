package nl.daan.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import java.util.Collections;
import java.util.List;

import static nl.daan.KitPvP.color;

public abstract class CommandBase implements CommandExecutor, TabCompleter {

    private String command, permission;

    public CommandBase(String command, String permission){
        this.command = command;
        this.permission = permission;
    }

    public CommandBase(String command){
        this.command = command;
    }

    public String getCommand() {
        return command;
    }

    public String getPermission() {
        return permission;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (this.getPermission() != null){
            String NO_PERMISSION = color("&cSorry, je kan dit command niet gebruiken.");
            if (this.getPermission().equalsIgnoreCase("op") && !sender.isOp()){
                sender.sendMessage(NO_PERMISSION);
                return true;
            } else if (!sender.hasPermission(this.getPermission())){
                sender.sendMessage(NO_PERMISSION);
                return true;
            }
        }
        this.executeCommand(sender, cmd, label, args);
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        if (this.getPermission() != null) {
            if (this.getPermission().equalsIgnoreCase("op")){
                if (!sender.isOp()) return Collections.emptyList();
            } else if (!sender.hasPermission(this.getPermission())) {
                return Collections.emptyList();
            }
        }
        return this.tabComplete(sender, cmd, label, args);
    }

    public abstract void executeCommand(CommandSender sender, Command cmd, String label, String[] args);

    public abstract List<String> tabComplete(CommandSender sender, Command cmd, String label, String[] args);

}
