package nl.daan.commands;

import nl.daan.Backup;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import java.util.List;

import static nl.daan.KitPvP.color;

public class BackupCommand extends CommandBase {

    public BackupCommand(){
        super("backup", "op");
    }

    @Override
    public void executeCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Backup backup = new Backup();
        backup.makeBackup();
        sender.sendMessage(color("&aBackup van alle plugin data gemaakt."));
    }

    @Override
    public List<String> tabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return null;
    }
}
