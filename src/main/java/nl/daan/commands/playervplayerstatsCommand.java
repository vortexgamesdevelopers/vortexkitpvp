package nl.daan.commands;

import nl.daan.data.Data;
import nl.daan.data.objects.player.DataPlayer;
import nl.daan.data.objects.player.DataPlayerSumo;
import nl.daan.utils.ItemstackFactory;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.List;
import java.util.UUID;

import static nl.daan.KitPvP.color;

public class playervplayerstatsCommand extends CommandBase {

    public playervplayerstatsCommand(){
        super("playervplayerstats");
    }

    @Override
    public void executeCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) return;
        Player player = (Player) sender;
        if (args.length == 1){
            OfflinePlayer target = Bukkit.getOfflinePlayer(UUID.fromString(args[0]));
            if (target.getUniqueId().equals(player.getUniqueId())){
                player.sendMessage(color("&cJe kan niet je eigen stats openen."));
                return;
            }
            Inventory inventory = Bukkit.createInventory(null, 27, color("&aStats tegen " + target.getName()));
            DataPlayer playerData = Data.getPlayer(player);
            if (playerData.getPlayerKills().containsKey(target.getUniqueId())){
                inventory.setItem(10, new ItemstackFactory(Material.DIAMOND_SWORD).setDisplayName("&cKills: &e" + playerData.getPlayerKills().get(target.getUniqueId())));
            }
            if (playerData.getPlayerDeaths().containsKey(target.getUniqueId())){
                inventory.setItem(11, new ItemstackFactory(Material.ROTTEN_FLESH).setDisplayName("&cDeaths: &e" + playerData.getPlayerDeaths().get(target.getUniqueId())));
            }
            if (playerData.getSumo() != null){
                DataPlayerSumo dataPlayerSumo = playerData.getSumo();
                if (dataPlayerSumo.getPlayerKills().containsKey(target.getUniqueId())){
                    inventory.setItem(12, new ItemstackFactory(Material.DIAMOND_SWORD).setDisplayName("&cSumo Kills: &e" + dataPlayerSumo.getPlayerKills().get(target.getUniqueId())));
                }
                if (dataPlayerSumo.getPlayerDeaths().containsKey(target.getUniqueId())){
                    inventory.setItem(13, new ItemstackFactory(Material.ROTTEN_FLESH).setDisplayName("&cSumo Deaths: &e" + dataPlayerSumo.getPlayerDeaths().get(target.getUniqueId())));
                }
            }
            player.openInventory(inventory);
        }
    }

    @Override
    public List<String> tabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return null;
    }
}
