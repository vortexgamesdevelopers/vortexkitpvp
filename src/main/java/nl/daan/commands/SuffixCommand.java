package nl.daan.commands;

import nl.daan.data.Data;
import nl.daan.data.objects.player.DataPlayer;
import nl.daan.types.Suffix;
import nl.daan.utils.ItemstackFactory;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.List;

import static nl.daan.KitPvP.color;

public class SuffixCommand extends CommandBase {

    public SuffixCommand(){
        super("suffix");
    }

    @Override
    public void executeCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) return;
        Player player = (Player) sender;
        DataPlayer dataPlayer = Data.getPlayer(player);
        if (args.length == 0){
            Inventory inventory = Bukkit.createInventory(null, 27, color("&aSelecteer je suffix"));
            for (Suffix suffix : Suffix.values()){
                if (suffix.getPrefix().equals("") || suffix.getPrefix() == null){
                    inventory.addItem(new ItemstackFactory(Material.ANVIL).setDisplayName("&cVerwijder je suffix"));
                } else {
                    if (dataPlayer.getAvailableSuffix().contains(suffix)){
                        inventory.addItem(new ItemstackFactory(Material.SIGN).setDisplayName(suffix.getPrefix()));
                    } else {
                        inventory.addItem(new ItemstackFactory(Material.BARRIER).setDisplayName(suffix.getPrefix()));
                    }
                }
            }
            player.openInventory(inventory);
        } else {
            if (player.isOp()){
                if (args[0].equalsIgnoreCase("add")){
                    OfflinePlayer target = Bukkit.getOfflinePlayer(args[1]);
                    DataPlayer targetData = Data.getPlayer(target);
                    if (targetData == null){
                        player.sendMessage(color("&cSpeler niet gevonden"));
                    } else {
                        Suffix suffix = Suffix.valueOf(args[2].toUpperCase());
                        targetData.addSuffix(suffix);
                        player.sendMessage(color("&aSuffix toegevoegd."));
                    }
                } else if (args[0].equalsIgnoreCase("remove")){
                    OfflinePlayer target = Bukkit.getOfflinePlayer(args[1]);
                    DataPlayer targerData = Data.getPlayer(target);
                    if (targerData == null){
                        player.sendMessage(color("&cSpeler niet gevonden"));
                    } else {
                        Suffix suffix = Suffix.valueOf(args[2].toUpperCase());
                        dataPlayer.removeSuffix(suffix);
                        player.sendMessage(color("&aSuffix verwijderd."));
                    }
                } else {
                    player.sendMessage(color("Niet gevonden."));
                }
            }
        }
    }

    @Override
    public List<String> tabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return null;
    }
}
