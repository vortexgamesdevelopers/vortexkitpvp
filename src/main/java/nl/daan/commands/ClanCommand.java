package nl.daan.commands;

import nl.daan.data.Data;
import nl.daan.data.objects.clan.Clan;
import nl.daan.data.objects.player.DataPlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.List;

import static nl.daan.KitPvP.color;

public class ClanCommand extends CommandBase {

    public ClanCommand(){
        super("clan", "op");
    }

    @Override
    public void executeCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) return;
        Player player = (Player) sender;
        if (args.length == 0){
            player.chat("/clan help");
        } else {
            DataPlayer dataPlayer = Data.getPlayer(player);
            if (args[0].equalsIgnoreCase("create")){
                String name = args[1];
                if (dataPlayer.getClan() != null){
                    player.sendMessage(color("&cJe zit al in een clan."));
                } else {
                    if (Data.clans.stream().anyMatch(i -> i.getName().toLowerCase().equals(name.toLowerCase()))){
                        player.sendMessage(color("&cEr is al een clan met de naam \"" + name + "\"."));
                    } else {
                        Clan clan = new Clan(name, player.getUniqueId());
                        Data.clans.add(clan);
                        player.sendMessage(color("&aJe hebt succesvol de clan \"" + name + "\" aangemaakt."));
                        //Bukkit.broadcastMessage(color("&aDe clan \"&2" + name + "&a\" is succesvol aangemaakt door &2" + player.getName() + "&a."));
                    }
                }
            } else if (args[0].equalsIgnoreCase("disband")){
                if (dataPlayer.getClan() == null){
                    player.sendMessage(color("&cJe zit niet in een clan"));
                } else {
                    Clan clan = Data.getClan(dataPlayer.getClan());
                    if (!clan.getOwner().equals(player.getUniqueId())){
                        player.sendMessage(color("&cAlleen de owner van deze clan kan hem verwijderen."));
                    } else {
                        clan.disband();
                    }
                }
            } else if (args[0].equalsIgnoreCase("invite")){

            } else if (args[0].equalsIgnoreCase("deinvite")){

            } else if (args[0].equalsIgnoreCase("info")){

            } else if (args[0].equalsIgnoreCase("setdescription")){

            } else if (args[0].equalsIgnoreCase("rename")){

            } else if (args[0].equalsIgnoreCase("chat")){

            } else if (args[0].equalsIgnoreCase("accept")){

            } else if (args[0].equalsIgnoreCase("promote")){

            } else if (args[0].equalsIgnoreCase("demote")){

            } else if (args[0].equalsIgnoreCase("kick")){

            } else if (args[0].equalsIgnoreCase("quest")){

            } else if (args[0].equalsIgnoreCase("settings")){

            } else if (args[0].equalsIgnoreCase("transfer")){

            } else if (args[0].equalsIgnoreCase("list")){

            } else if (args[0].equalsIgnoreCase("invites")){

            }
        }
    }

    @Override
    public List<String> tabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 1){
            return Arrays.asList("create", "disband", "invite", "deinvite", "setdescription", "rename", "chat", "info", "help", "accept", "promote", "demote", "kick", "quest", "settings", "transfer", "list", "invites");
        }
        return null;
    }
}
