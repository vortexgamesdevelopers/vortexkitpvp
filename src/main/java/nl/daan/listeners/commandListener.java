package nl.daan.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import java.util.Arrays;
import java.util.List;

import static nl.daan.KitPvP.color;

public class commandListener implements Listener {

    public static List<String> commands = Arrays.asList("powertool", "epowertool", "ept", "pt");

    @EventHandler
    public void onPreCommand(PlayerCommandPreprocessEvent event){
        String[] words = event.getMessage().split(" ");
        String command = words[0].replaceAll("/", "");
        if (commands.contains(command.toLowerCase())){
            event.setCancelled(true);
            event.getPlayer().sendMessage(color("&cDit command is disabled."));
        }
    }

}
