package nl.daan.listeners;

import nl.daan.data.Data;
import nl.daan.data.objects.player.DataPlayer;
import nl.daan.types.PlayState;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.FishHook;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

public class DamageListener implements Listener {

    @EventHandler
    public void onPlayerDamagePlayer(EntityDamageByEntityEvent event){
        if (event.getDamager() instanceof Player && event.getEntity() instanceof Player){
            Player damager = (Player) event.getDamager();
            Player target = (Player) event.getEntity();
            DataPlayer damagerPlayer = Data.getPlayer(damager);
            DataPlayer targetPlayer = Data.getPlayer(target);
            if (!damagerPlayer.getPlayState().equals(PlayState.PLAYING) || !targetPlayer.getPlayState().equals(PlayState.PLAYING)) {
                event.setCancelled(true);
                return;
            }
            if (!validLocation(damager) || !validLocation(target)){
                event.setCancelled(true);
                return;
            }
            damagerPlayer.setDamageDealt(damagerPlayer.getDamageDealt() + event.getDamage());
            targetPlayer.setDamageTaken(targetPlayer.getDamageTaken() + event.getDamage());
            return;
        }
        if (event.getEntity() instanceof Player){
            if (event.getDamager() instanceof Arrow){
                Arrow arrow = (Arrow) event.getDamager();
                Player shooter =  (Player) arrow.getShooter();
                Player target = (Player) event.getEntity();
                if (!validLocation(shooter) || !validLocation(target)){
                    event.setCancelled(true);
                    return;
                }
                return;
            }
            if (event.getDamager() instanceof FishHook){
                FishHook fishHook = (FishHook) event.getDamager();
                Player shooter =  (Player) fishHook.getShooter();
                Player target = (Player) event.getEntity();
                if (!validLocation(shooter) || !validLocation(target)){
                    event.setCancelled(true);
                    return;
                }
                return;
            }
            return;
        }
        return;
    }

    @EventHandler
    public void onRandomDamage(EntityDamageEvent event){
        if (event.getEntity() instanceof Player){
            Player player = (Player) event.getEntity();
            if (event.getCause() == EntityDamageEvent.DamageCause.FALL){
                event.setCancelled(true);
            }
        }
    }

    private boolean validLocation(Player player){
        Block block = player.getLocation().getBlock().getLocation().add(0, -player.getLocation().getBlockY(), 0).getBlock();
        Material material = block.getType();
        if (material.equals(Material.DIAMOND_BLOCK)) return false;
        return true;
    }

}
