package nl.daan.listeners;

import nl.daan.data.Data;
import nl.daan.data.objects.player.DataPlayer;
import nl.daan.data.objects.kit.Kit;
import nl.daan.data.objects.kit.KitItem;
import nl.daan.data.objects.kit.ModifiedKit;
import nl.daan.managers.KitEditorManager;
import nl.daan.types.ArmorSlot;
import nl.daan.types.PlayState;
import nl.daan.types.Suffix;
import nl.daan.utils.ItemstackFactory;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import java.util.Arrays;
import java.util.HashMap;

import static nl.daan.KitPvP.color;

public class InventoryClickListener implements Listener {

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event){
        Player player = (Player) event.getWhoClicked();
        DataPlayer dataPlayer = Data.getPlayer(player);
        switch (dataPlayer.getPlayState()){
            case PLAYING:
                break;
            case SPECTATING:
                break;
            case IDLE:
                if (event.getInventory().getName().equalsIgnoreCase(color("&aSelecteer je kit."))){
                    event.setCancelled(true);
                    if (event.getCurrentItem() != null && !event.getCurrentItem().getType().equals(Material.AIR)){
                        if (!event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(color("&cGeen kits gevonden"))){
                            Kit kit = getKitFromDisplayName(event.getCurrentItem().getItemMeta().getDisplayName().substring(2));
                            /*if (kit.getPermission() != null) {
                                if (!player.hasPermission(kit.getPermission())){
                                    player.sendMessage(color("&cJe hebt geen permissions voor deze kit."));
                                    return;
                                }
                            }*/
                            appendKit(player, kit);
                            dataPlayer.setPlayState(PlayState.PLAYING);
                            player.sendMessage(color("&7Je hebt de kit &e\"" + kit.getDisplayName() + "&e\" &7gekozen!"));
                            player.closeInventory();
                            dataPlayer.setLastUsedKit(kit.getUuid());
                        }
                    }
                }
                if (event.getInventory().getName().equals(color("&aStatistics"))){
                    event.setCancelled(true);
                }
                if (event.getInventory().getName().equals(color("&aShop"))){
                    event.setCancelled(true);
                    String NOT_ENOUGH_GOLD = color("&cJe hebt niet genoeg gold voor dit item.");
                    if (event.getSlot() == 10){
                        double gold = dataPlayer.getGold();
                        if (gold >= 150){
                            dataPlayer.addPurchasedItem(new KitItem(new ItemstackFactory(Material.GOLDEN_APPLE, 1).setDisplayName("&aGolden Apple")));
                            player.sendMessage(color("&aJe zal de extra golden apple ontvangen als je je kit hebt gekozen."));
                            dataPlayer.setGold(dataPlayer.getGold() - 150);
                        } else {
                            player.closeInventory();
                            player.sendMessage(NOT_ENOUGH_GOLD);
                        }
                    }
                    if (event.getSlot() == 11){
                        double gold = dataPlayer.getGold();
                        if (gold >= 2500 && !dataPlayer.isEnderchest()){
                            dataPlayer.setEnderchest(true);
                            player.sendMessage(color("&aJe kan nu de enderchest gebruiken in de spawn."));
                            player.closeInventory();
                            dataPlayer.setGold(dataPlayer.getGold() - 2500);
                        } else {
                            if (dataPlayer.isEnderchest()){
                                player.sendMessage(color("&cJe hebt de enderchest al!"));
                            } else {
                                player.sendMessage(NOT_ENOUGH_GOLD);
                            }
                            player.closeInventory();
                        }
                    }
                }
                break;
        }
        if (event.getInventory().getName().startsWith(color("&aStats tegen "))){
            event.setCancelled(true);
        }
        if (event.getInventory().getName().equals(color("&aSelecteer een kit"))){
            event.setCancelled(true);
            if (event.getCurrentItem() != null && !event.getCurrentItem().getType().equals(Material.AIR)){
                Kit kit = getKitFromDisplayName(event.getCurrentItem().getItemMeta().getDisplayName().substring(2));
                KitEditorManager kitEditorManager = new KitEditorManager(player, kit);
                kitEditorManager.init();
                dataPlayer.setKitEditorManager(kitEditorManager);
            }
        }
        if (event.getInventory().getName().equals(color("&aSelecteer je suffix"))) {
            event.setCancelled(true);
            if (event.getCurrentItem() != null && event.getCurrentItem().getType().equals(Material.SIGN)) {
                System.out.println(ChatColor.stripColor(event.getCurrentItem().getItemMeta().getDisplayName()));
                Suffix suffix = Arrays.stream(Suffix.values()).filter(i -> i.getPrefix().contains(ChatColor.stripColor(event.getCurrentItem().getItemMeta().getDisplayName()))).findFirst().get();
                dataPlayer.setSuffix(suffix);
                player.sendMessage(color("&aJe hebt de suffix " + event.getCurrentItem().getItemMeta().getDisplayName() + " &ageselecteerd!"));
                player.closeInventory();
            } else if (event.getCurrentItem() != null && event.getCurrentItem().getType().equals(Material.ANVIL)){
                player.closeInventory();
                if (dataPlayer.getSuffix().equals(Suffix.NONE)){
                    player.sendMessage(color("&cJe hebt nog geen suffix!"));
                } else {
                    dataPlayer.setSuffix(Suffix.NONE);
                    player.sendMessage(color("&aJe hebt je suffix weg gedaan."));
                }
            } else if (event.getCurrentItem() != null && event.getCurrentItem().getType().equals(Material.BARRIER)){
                player.sendMessage(color("&cJe hebt deze suffix niet"));
                player.closeInventory();
            }
        }
    }

    public static void setItems(Player player, HashMap<Integer, KitItem> items, HashMap<ArmorSlot, KitItem> armor){
        player.getInventory().clear();
        player.getInventory().setArmorContents(null);
        for (Integer slot : items.keySet()){
            KitItem item = items.get(slot);
            player.getInventory().setItem(slot, item.getItemStack());
        }
        for (ArmorSlot armorSlot : armor.keySet()){
            switch (armorSlot){
                case HELMET:
                    player.getInventory().setHelmet(armor.get(armorSlot).getItemStack());
                    break;
                case CHESTPLATE:
                    player.getInventory().setChestplate(armor.get(armorSlot).getItemStack());
                    break;
                case LEGGING:
                    player.getInventory().setLeggings(armor.get(armorSlot).getItemStack());
                    break;
                case BOOTS:
                    player.getInventory().setBoots(armor.get(armorSlot).getItemStack());
                    break;
            }
        }
    }

    public static void appendKit(Player player, Kit kit){
        DataPlayer dataPlayer = Data.getPlayer(player);
        if (dataPlayer.getKits() != null){
            if (dataPlayer.getKits().size() != 0){
                if (dataPlayer.getKits().containsKey(kit.getUuid())){
                    ModifiedKit modifiedKit = dataPlayer.getKits().get(kit.getUuid());
                    setItems(player, modifiedKit.getItems(), modifiedKit.getArmor());
                }
            } else {
                setItems(player, kit.getItems(), kit.getArmor());
            }
        } else {
            setItems(player, kit.getItems(), kit.getArmor());
        }
        if (dataPlayer.getPurchasesItems() != null){
            if (dataPlayer.getPurchasesItems().size() != 0){
                for (KitItem kitItem : dataPlayer.getPurchasesItems()){
                    player.getInventory().addItem(kitItem.getItemStack());
                }
                dataPlayer.clearPurchasedItems();
            }
        }
    }

    private Kit getKitFromDisplayName(String displayName){
        return Data.kits.stream().filter(i -> i.getDisplayName().equals(displayName)).findFirst().get();
    }

}
