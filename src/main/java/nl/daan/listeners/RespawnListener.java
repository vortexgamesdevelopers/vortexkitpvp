package nl.daan.listeners;

import nl.daan.KitPvP;
import nl.daan.data.Data;
import nl.daan.data.objects.player.DataPlayer;
import nl.daan.managers.InventoryManager;
import nl.daan.types.PlayState;
import nl.daan.utils.LocationSerializer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;

public class RespawnListener implements Listener {

    @EventHandler
    public void onRespawn(PlayerRespawnEvent event){
        Player player = event.getPlayer();
        InventoryManager.resetInventory(player);
        DataPlayer dataPlayer = Data.getPlayer(player);
        dataPlayer.setPlayState(PlayState.IDLE);
        Bukkit.getScheduler().scheduleSyncDelayedTask(KitPvP.getInstance(), new Runnable() {
            @Override
            public void run() {
                if (!KitPvP.getInstance().getConfig().getString("spawn").equals("None")){
                    player.teleport(LocationSerializer.deSerialize(KitPvP.getInstance().getConfig().getString("spawn")));
                }
            }
        }, 2L);
    }

}
