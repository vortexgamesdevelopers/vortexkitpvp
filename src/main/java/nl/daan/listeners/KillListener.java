package nl.daan.listeners;

import nl.daan.data.Data;
import nl.daan.data.objects.player.DataPlayer;
import nl.daan.managers.LevelManager;
import nl.daan.utils.ItemstackFactory;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import java.text.DecimalFormat;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import static nl.daan.KitPvP.color;

public class KillListener implements Listener {

    @EventHandler
    public void onKill(PlayerDeathEvent event){
        Player death = event.getEntity();
        DataPlayer deathPlayer = Data.getPlayer(death);
        int deathKillstreak = deathPlayer.getKillstreak();
        deathPlayer.setDeaths(deathPlayer.getDeaths() + 1);
        deathPlayer.setKillstreak(0);
        Player killer = death.getKiller();
        event.setDeathMessage(null);
        if (killer != null){
            if (deathKillstreak >= 5){
                Bukkit.broadcastMessage(color("&4" + killer.getName() + " &cheeft de killstreak van &4" + death.getName() + "&c beëindigd!"));
            }
            DataPlayer killerPlayer = Data.getPlayer(killer);
            killerPlayer.appendKill(death);
            deathPlayer.appendDeaths(killer);
            killerPlayer.setKills(killerPlayer.getKills() + 1);
            killerPlayer.setKillstreak(killerPlayer.getKillstreak() + 1);
            DecimalFormat decimalFormat = new DecimalFormat("##0.#");
            death.sendMessage(color("&2" + killer.getName() + " &aheeft nog &2" + decimalFormat.format(killer.getHealth()) + " &ahealth."));
            applyHealth(killer);
            applyRewards(killer);
            LevelManager.calculateLevelFromExp(killer);
            killer.sendMessage(color("&a&lKILL! &7Je hebt <death> gekilled.".replaceAll("<death>", death.getName())));
            death.sendMessage(color("&c&lDEATH! &7Je bent vermoord door <killer>".replaceAll("<killer>", killer.getName())));
            switch (killerPlayer.getKillstreak()){
                case 5:
                    broadcastKillstreak(5, killer);
                    break;
                case 10:
                    broadcastKillstreak(10, killer);
                    break;
                case 15:
                    broadcastKillstreak(15, killer);
                    break;
                case 20:
                    broadcastKillstreak(20, killer);
                    break;
                default:
                    break;
            }
        } else {
            death.sendMessage(color("&c&lDEATH! &7Je bent dood gegaan dooor een onbekende reden."));
        }
    }

    private void broadcastKillstreak(int streak, Player player){
        Bukkit.broadcastMessage(color("&2" + player.getName() + "&a heeft een &2" + streak + " &akillstreak!"));
    }

    private void applyHealth(Player killer){
        double currentHealth = killer.getHealth();
        currentHealth += 10;
        if (currentHealth > 20) currentHealth = 20;
        killer.setHealth(currentHealth);
    }

    private void applyRewards(Player killer){
        Random random = new Random();
        DataPlayer dataPlayer = Data.getPlayer(killer);
        dataPlayer.setGold(dataPlayer.getGold() + ThreadLocalRandom.current().nextDouble(5, 20));
        dataPlayer.setExp(dataPlayer.getExp() + random.nextInt(30));
        int rand = random.nextInt(20);
        switch (rand){
            case 1:
                killer.getInventory().addItem(new ItemstackFactory(Material.GOLDEN_APPLE));
                break;
            case 2:
                if (killer.getInventory().contains(Material.BOW)) {
                    killer.getInventory().addItem(new ItemstackFactory(Material.ARROW, 5));
                } else {
                    killer.getInventory().addItem(new ItemstackFactory(Material.BOW, 1));
                    killer.getInventory().addItem(new ItemstackFactory(Material.ARROW, 5));
                }
                break;
            case 3:
                killer.getInventory().addItem(new ItemstackFactory(Material.GOLDEN_APPLE));
                break;
            default:
                break;
        }
    }

}
