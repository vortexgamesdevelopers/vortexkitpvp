package nl.daan.listeners;

import nl.daan.data.Data;
import nl.daan.data.objects.player.DataPlayer;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

import static nl.daan.KitPvP.color;

public class SignClickListener implements Listener {

    @EventHandler
    public void onSignClick(PlayerInteractEvent event){
        Player player = event.getPlayer();
        DataPlayer dataPlayer = Data.getPlayer(player);
        if (event.getAction().toString().contains("BLOCK")){
            if (event.getClickedBlock() != null){
                if (event.getClickedBlock().getType().equals(Material.SIGN) || event.getClickedBlock().getType().equals(Material.WALL_SIGN)){
                    if (dataPlayer.getKitEditorManager() != null){
                        Sign sign = (Sign) event.getClickedBlock().getState();
                        if (sign.getLine(0).equals(color("&aSave Kit"))){
                            dataPlayer.getKitEditorManager().save();
                        }
                        if (sign.getLine(0).equals(color("&cReset Kit"))){
                            dataPlayer.getKitEditorManager().reset();
                        }
                    }
                }
            }
        }

    }

}
