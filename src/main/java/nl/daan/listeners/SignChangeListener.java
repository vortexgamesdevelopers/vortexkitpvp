package nl.daan.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;

import static nl.daan.KitPvP.color;

public class SignChangeListener implements Listener {

    @EventHandler
    public void onSignChange(SignChangeEvent event){
        if (event.getLine(0).equalsIgnoreCase("[Save Kit]")){
            event.setLine(0, color("&aSave Kit"));
        }
        if (event.getLine(0).equalsIgnoreCase("[Reset Kit]")){
            event.setLine(0, color("&cReset Kit"));
        }
    }

}
