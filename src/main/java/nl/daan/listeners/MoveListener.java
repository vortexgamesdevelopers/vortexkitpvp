package nl.daan.listeners;

import nl.daan.api.events.RegionChangeEvent;
import nl.daan.data.Data;
import nl.daan.data.objects.player.DataPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class MoveListener implements Listener {

    @EventHandler
    public void onMove(PlayerMoveEvent event){
        Player player = event.getPlayer();
        DataPlayer dataPlayer = Data.getPlayer(player);
        if (event.getFrom().getBlock().getLocation() != event.getTo().getBlock().getLocation()){
            dataPlayer.setBlockMoved(dataPlayer.getBlockMoved() + 1);
            RegionChangeEvent regionChangeEvent = new RegionChangeEvent(player, event.getTo(), event.getFrom());
            Bukkit.getPluginManager().callEvent(regionChangeEvent);
            regionChangeEvent.execute();
        }
    }

}
