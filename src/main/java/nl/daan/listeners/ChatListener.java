package nl.daan.listeners;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import nl.daan.data.Data;
import nl.daan.data.objects.player.DataPlayer;
import nl.daan.types.Suffix;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import static nl.daan.KitPvP.color;

public class ChatListener implements Listener {

    @EventHandler(ignoreCancelled = true)
    public void onChat(AsyncPlayerChatEvent event){
        Player player = event.getPlayer();
        DataPlayer dataPlayer = Data.getPlayer(player);
        dataPlayer.setChatMessagesSend(dataPlayer.getChatMessagesSend() + 1);
        event.setCancelled(true);
        for (Player all : Bukkit.getOnlinePlayers()){
            sendFormatMessage(player, all, event.getMessage());
        }
        event.setFormat(color("&c[&e" + dataPlayer.getKills() + "&c] %s&8: &7") + "%s");
    }

    private void sendFormatMessage(Player player, Player to, String message){
        DataPlayer dataPlayer = Data.getPlayer(player);
        String suffix = "";
        if (dataPlayer.getSuffix() != Suffix.NONE){
            suffix += " ";
            suffix += color(dataPlayer.getSuffix().getPrefix());
        }
        to.spigot().sendMessage(
                new ComponentBuilder("[")
                        .color(ChatColor.RED)
                        .append(String.valueOf(dataPlayer.getLevel()))
                        .color(ChatColor.YELLOW)
                        .append("] ")
                        .color(ChatColor.RED)
                        .append(player.getName())
                        .event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/playervplayerstats " + player.getUniqueId()))
                        .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                                new ComponentBuilder(
                                        "Klik hier om de stats tussen jou en " + player.getName() + " te zien.")
                                        .color(ChatColor.GREEN)
                                        .create()
                                )
                        )
                        .append(suffix)
                        .reset()
                        .append(":")
                        .color(ChatColor.DARK_GRAY)
                        .append(" " + message)
                        .color(ChatColor.RESET)
                        .create()
        );
    }

    private void laterAvailable(Player player, String message){
        player.spigot().sendMessage(
                new ComponentBuilder("Op het moment is deze functie nog niet mogelijk!\nJoin onze discord voor een update wanneer deze functie er is.\nKlik ")
                        .color(ChatColor.GREEN)
                        .append("HIER")
                        .event(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://discord.gg/S4K7en9"))
                        .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Klik hier om de discord te joinen").color(ChatColor.GREEN).create()))
                        .color(ChatColor.GREEN)
                        .bold(true)
                        .underlined(true)
                        .append(" om onze discord te joinen.")
                        .reset()
                        .color(ChatColor.GREEN)
                        .create()
        );
    }

}
