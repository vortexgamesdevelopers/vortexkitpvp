package nl.daan.listeners;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.*;
import nl.daan.data.Data;
import nl.daan.data.objects.player.DataPlayer;
import nl.daan.managers.InventoryManager;
import nl.daan.managers.SumoManager;
import nl.daan.types.PlayState;
import nl.daan.utils.ItemstackFactory;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static nl.daan.KitPvP.color;

public class InteractListener implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent event){
        Player player = event.getPlayer();
        if (player.getGameMode().equals(GameMode.CREATIVE)) return;
        DataPlayer dataPlayer = Data.getPlayer(player);
        PlayState playState = dataPlayer.getPlayState();
        if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) dataPlayer.setRightClicks(dataPlayer.getRightClicks() + 1);
        if (event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK) dataPlayer.setLeftClicks(dataPlayer.getLeftClicks() + 1);
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK){
            if (event.getClickedBlock() != null){
                if (event.getClickedBlock().getType().equals(Material.ENDER_CHEST)){
                    if (!dataPlayer.isEnderchest()) {
                        event.setCancelled(true);
                        player.sendMessage(color("&7De &5&lEnderchest &7is nu nog vergrendeld!\n&7Unlock hem in de shop!"));
                    }
                }
            }
        }
        switch (playState){
            case IDLE:
                switch (player.getItemInHand().getType()){
                    case COMPASS:
                        if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK){
                            InventoryManager.kitSelector(player);
                        } else {
                            if (Data.getPlayer(player).getLastUsedKit() != null && Data.kits.stream().anyMatch(i -> i.getUuid().equals(Data.getPlayer(player).getLastUsedKit()))){
                                InventoryClickListener.appendKit(player, Data.kits.stream().filter(i -> i.getUuid().equals(Data.getPlayer(player).getLastUsedKit())).findFirst().get());
                            }
                        }
                        break;
                    case SKULL_ITEM:
                        player.openInventory(getStatisticsInventory(player));
                        break;
                    case BARRIER:
                        laterAvailable(player);
                        break;
                    case EMERALD:
                        player.openInventory(getShopInventory(player));
                        break;
                    case STICK:
                        SumoManager.join(player);
                        break;
                    case CHEST:
                        Inventory inventory = Bukkit.createInventory(null, 27, color("&aSelecteer een kit"));
                        if (Data.kits.size() > 0){
                            Data.kits.stream().forEach(kit -> {
                                ItemStack itemStack = kit.getGuiItem().getItemStack();
                                ItemMeta itemMeta = itemStack.getItemMeta();
                                itemMeta.setDisplayName(color("&f" + kit.getDisplayName()));
                                List<String> lore;
                                if (itemMeta.getLore()==null) {
                                    lore = new ArrayList<>();
                                } else {
                                    lore = itemMeta.getLore();
                                }
                                lore.add(" ");
                                for (String line : kit.getKitDescription()){
                                    lore.add(color("&f" + line));
                                }
                                itemMeta.setLore(lore);
                                itemStack.setItemMeta(itemMeta);
                                inventory.addItem(itemStack);
                            });
                        }
                        player.openInventory(inventory);
                        break;
                    default:
                        break;
                }
                break;
            case SPECTATING:
                switch (player.getItemInHand().getType()){
                    case BED:
                        break;
                    default:
                        break;
                }
                break;
            case PLAYING:
                switch (player.getItemInHand().getType()){
                    default:
                        break;
                }
                break;
        }
    }

    private void laterAvailable(Player player){
        player.spigot().sendMessage(
            new ComponentBuilder("Op het moment is deze functie nog niet mogelijk!\nJoin onze discord voor een update wanneer deze functie er is.\nKlik ")
                    .color(ChatColor.GREEN)
                    .append("HIER")
                    .event(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://discord.gg/S4K7en9"))
                    .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Klik hier om de discord te joinen").color(ChatColor.GREEN).create()))
                    .color(ChatColor.GREEN)
                    .bold(true)
                    .underlined(true)
                    .append(" om onze discord te joinen.")
                    .reset()
                    .color(ChatColor.GREEN)
                    .create()
        );
    }

    public static Inventory getShopInventory(Player player){
        DataPlayer dataPlayer = Data.getPlayer(player);
        Inventory inventory = Bukkit.createInventory(null, 27, color("&aShop"));
        double gold = dataPlayer.getGold();
        if (gold >= 150){
            inventory.setItem(10, new ItemstackFactory(Material.GOLDEN_APPLE, 1).setDisplayName("&aGolden Apple").setLore(Arrays.asList(color("&cPrijs: &e150"))));
        } else {
            inventory.setItem(10, new ItemstackFactory(Material.BARRIER, 1).setDisplayName("&cGolden Apple").setLore(Arrays.asList(color("&cPrijs: &e150"))));
        }
        if (gold >= 2500){
            inventory.setItem(11, new ItemstackFactory(Material.ENDER_CHEST, 1).setDisplayName("&aEnderchest").setLore(Arrays.asList(color("&cPrijs: &e2500"))));
        } else {
            inventory.setItem(11, new ItemstackFactory(Material.BARRIER, 1).setDisplayName("&cEnderchest").setLore(Arrays.asList(color("&cPrijs: &e2500"))));
        }
        return inventory;
    }

    private Inventory getStatisticsInventory(Player player){
        Inventory inventory = Bukkit.createInventory(null, 27, color("&aStatistics"));
        DataPlayer dataPlayer = Data.getPlayer(player);
        inventory.setItem(10, new ItemstackFactory(Material.DIAMOND_SWORD).setDisplayName("&cKills: &e" + dataPlayer.getKills()));
        inventory.setItem(11, new ItemstackFactory(Material.ROTTEN_FLESH).setDisplayName("&cDeaths: &e" + dataPlayer.getDeaths()));
        inventory.setItem(12, new ItemstackFactory(Material.SIGN).setDisplayName("&cChat Berichten: &e" + dataPlayer.getChatMessagesSend()));
        inventory.setItem(13, new ItemstackFactory(Material.BARRIER).setDisplayName("&cKDR: &e" + getKDR(dataPlayer.getKills(), dataPlayer.getDeaths())));
        return inventory;
    }

    private String getKDR(double kills, double deaths){
        double unFormatted = kills / deaths;
        DecimalFormat decimalFormat = new DecimalFormat("##0.##");
        return decimalFormat.format(unFormatted);
    }

}