package nl.daan.listeners;

import nl.daan.KitPvP;
import nl.daan.data.Data;
import nl.daan.data.objects.player.DataPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class QuitListener implements Listener {

    @EventHandler
    public void onQuit(PlayerQuitEvent event){
        Player player = event.getPlayer();
        DataPlayer dataPlayer = Data.getPlayer(player);
        KitPvP.getScoreboards().removePlayer(player, dataPlayer.getRank());
    }

}
