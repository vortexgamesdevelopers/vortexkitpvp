package nl.daan.listeners;

import nl.daan.KitPvP;
import nl.daan.data.Data;
import nl.daan.data.objects.player.DataPlayer;
import nl.daan.managers.InventoryManager;
import nl.daan.types.PlayState;
import nl.daan.utils.LocationSerializer;
import nl.daan.utils.TabUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import static nl.daan.KitPvP.color;

public class JoinListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event){
        Player player = event.getPlayer();
        if (!Data.players.stream().anyMatch(i -> i.getUuid().equals(player.getUniqueId()))){
            DataPlayer dataPlayer = new DataPlayer(player.getUniqueId());
            Data.players.add(dataPlayer);
        }
        DataPlayer dataPlayer = Data.getPlayer(player);
        dataPlayer.setJoins(dataPlayer.getJoins() + 1);
        dataPlayer.setPlayState(PlayState.IDLE);
        dataPlayer.setKillstreak(0);
        InventoryManager.resetInventory(player);
        player.setDisplayName(color("&8[" + dataPlayer.getRank().getTabPrefix() + "&8] &7" + player.getName()));
        if (!KitPvP.getInstance().getConfig().getString("spawn").equals("None")){
            player.teleport(LocationSerializer.deSerialize(KitPvP.getInstance().getConfig().getString("spawn")));
        }
        TabUtils.sendHeaderFooter(player, color("\n&2&lVortex&a&lGames\n&7Welkom op &2VortexPvP\n"), color("\n&2play.vortexgames.nl\n"));
        KitPvP.getScoreboards().createScoreboard(player);
        KitPvP.getScoreboards().addPlayer(player, Data.getPlayer(player).getRank());
    }

}
