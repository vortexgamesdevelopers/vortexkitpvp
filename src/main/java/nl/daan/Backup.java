package nl.daan;

import com.google.gson.Gson;
import nl.daan.data.Data;
import nl.daan.data.objects.clan.Clan;
import nl.daan.data.objects.player.DataPlayer;
import nl.daan.data.objects.kit.Kit;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Backup {

    public List<DataPlayer> playerData = new ArrayList<>();
    public List<Kit> kitData = new ArrayList<>();
    public List<Clan> clanData = new ArrayList<>();

    public void makeBackup(){
        this.playerData = Data.players;
        this.kitData = Data.kits;
        this.clanData = Data.clans;
        Gson gson = new Gson();
        try {
            File file = new File(KitPvP.getInstance().getDataFolder() + File.separator + "Backup", new Date() + ".json");
            if (file.exists()) file.delete();
            file.createNewFile();
            FileUtils.write(file, gson.toJson(this));
        } catch (Exception e){
            e.printStackTrace();
        }
    }

}
