package nl.daan;

import nl.daan.data.Data;
import nl.daan.data.objects.player.DataPlayer;
import nl.daan.types.Rank;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.text.DecimalFormat;

import static nl.daan.KitPvP.color;

public class Scoreboards {

    //public Scoreboard scoreboard;

    public Scoreboards(){
        Bukkit.getOnlinePlayers().forEach(player -> {
            createScoreboard(player);
        });
    }

    public void createScoreboard(Player player){
        Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        for (Rank rank : Rank.values()){
            if (scoreboard.getTeam("rank-" + rank.toString()) == null) {
                scoreboard.registerNewTeam("rank-" + rank.toString());
            }
            scoreboard.getTeam("rank-" + rank.toString()).setPrefix(color("&8[" + rank.getTabPrefix() + "&8] &7"));
        }
        Objective objective = scoreboard.registerNewObjective("bar-" + player.getUniqueId().toString().substring(0, 5), "dummy");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.getScore(color("&f&m----------------")).setScore(16);
        Team kills = scoreboard.registerNewTeam("Kills: ");
        Team deaths = scoreboard.registerNewTeam("Deaths: ");
        Team killstreak = scoreboard.registerNewTeam("Killstreak: ");
        Team level = scoreboard.registerNewTeam("Level: ");
        Team prestige = scoreboard.registerNewTeam("Prestige: ");
        Team exp = scoreboard.registerNewTeam("Exp: ");
        Team gold = scoreboard.registerNewTeam("Gold: ");
        new BukkitRunnable(){
            @Override
            public void run() {
                if (!player.isOnline()) {
                    this.cancel();
                }
                DataPlayer dataPlayer = Data.getPlayer(player);
                objective.setDisplayName(color("&2&lVortex&a&lPvP"));
                kills.setSuffix(color("&a" + dataPlayer.getKills()));
                deaths.setSuffix(color("&a" + dataPlayer.getDeaths()));
                level.setSuffix(color("&a" + dataPlayer.getLevel()));
                prestige.setSuffix(color("&a" + dataPlayer.getPrestige()));
                killstreak.setSuffix(color("&a" + dataPlayer.getKillstreak()));
                exp.setSuffix(color("&a" + dataPlayer.getExp()));
                gold.setSuffix(color("&a" + getGold(player)));
                kills.setPrefix(color(" &f» "));
                deaths.setPrefix(color(" &f» "));
                killstreak.setPrefix(color(" &f» "));
                level.setPrefix(color(" &f» "));
                prestige.setPrefix(color(" &f» "));
                exp.setPrefix(color(" &f» "));
                gold.setPrefix(color(" &f» "));
            }
        }.runTaskTimer(KitPvP.getInstance(), 0, 10);
        kills.addEntry("Kills: ");
        deaths.addEntry("Deaths: ");
        killstreak.addEntry("Killstreak: ");
        level.addEntry("Level: ");
        prestige.addEntry("Prestige: ");
        exp.addEntry("Exp: ");
        gold.addEntry("Gold: ");
        objective.getScore("Kills: ").setScore(15);
        objective.getScore("Deaths: ").setScore(14);
        objective.getScore("Killstreak: ").setScore(13);
        objective.getScore("   ").setScore(12);
        objective.getScore("Level: ").setScore(11);
        objective.getScore("Prestige: ").setScore(10);
        objective.getScore("Exp: ").setScore(9);
        objective.getScore("Gold: ").setScore(8);
        objective.getScore(color("&r&f&m----------------")).setScore(7);
        objective.getScore(color("&2play.vortexgames.nl")).setScore(6);
        player.setScoreboard(scoreboard);
        for (Player all : Bukkit.getOnlinePlayers()){
            Team team = player.getScoreboard().getTeam("rank-" + Data.getPlayer(all).getRank().toString());
            if (!team.getPlayers().contains(all)){
                team.addPlayer(all);
            }
        }
        addPlayer(player, Data.getPlayer(player).getRank());
    }

    public void addPlayer(Player player, Rank rank){
        for (Player all : Bukkit.getOnlinePlayers()){
            Team team = all.getScoreboard().getTeam("rank-" + rank.toString());
            if (team != null) {
                if (!team.getPlayers().contains(all)){
                    team.addPlayer(player);
                }
            }
        }
    }

    public void removePlayer(Player player, Rank rank){
        Bukkit.getOnlinePlayers().forEach(onlinePlayer -> {
            onlinePlayer.getScoreboard().getTeam("rank-" + rank.toString()).removePlayer(player);
        });
    }

    private String getGold(Player player){
        DecimalFormat decimalFormat = new DecimalFormat("##0.###");
        return decimalFormat.format(Data.getPlayer(player).getGold());
    }

}
