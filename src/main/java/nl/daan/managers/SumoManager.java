package nl.daan.managers;

import nl.daan.KitPvP;
import nl.daan.data.Data;
import nl.daan.data.objects.player.DataPlayer;
import nl.daan.types.PlayState;
import nl.daan.utils.LocationSerializer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.ArrayList;
import java.util.List;

import static nl.daan.KitPvP.color;

public class SumoManager implements Listener {

    public static List<Player> sumoPlayers = new ArrayList<>();

    public static void join(Player player){
        sumoPlayers.add(player);
        player.getInventory().clear();
        player.getInventory().setArmorContents(null);
        player.teleport(LocationSerializer.deSerialize(KitPvP.getInstance().getConfig().getString("sumo")));
        DataPlayer dataPlayer = Data.getPlayer(player);
        dataPlayer.setPlayState(PlayState.PLAYING);
    }

    public static void leave(Player player){
        sumoPlayers.remove(player);
        InventoryManager.resetInventory(player);
        DataPlayer dataPlayer = Data.getPlayer(player);
        dataPlayer.setPlayState(PlayState.IDLE);
        player.setHealth(20);
        player.teleport(LocationSerializer.deSerialize(KitPvP.getInstance().getConfig().getString("spawn")));
        dataPlayer.getSumo().setDeaths(dataPlayer.getSumo().getDeaths() + 1);
        DataPlayer killerPlayer = Data.getPlayer(Bukkit.getOfflinePlayer(dataPlayer.getSumo().getLastDamager()));
        if (killerPlayer != null){
            killerPlayer.getSumo().setKills(killerPlayer.getSumo().getKills() + 1);
            killerPlayer.getSumo().appendKill(player);
            dataPlayer.getSumo().appendDeath(Bukkit.getPlayer(dataPlayer.getSumo().getLastDamager()));
            Player killer = Bukkit.getPlayer(dataPlayer.getSumo().getLastDamager());
            player.sendMessage(color("&c&lDEATH! &7Je bent in het water gehit door " + killer.getName() + "!"));
            killer.sendMessage(color("&a&lKILL! &7Je hebt " + player.getName() + "&7 in het water gehit!"));
        }
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event){
        Player player = (Player) event.getPlayer();
        if (sumoPlayers.contains(player)){
            if (player.getLocation().getBlock().getType() != null){
                if (player.getLocation().getBlockY() < 137){
                    leave(player);
                }
            }
        }
    }

    @EventHandler
    public void onDamage(EntityDamageByEntityEvent event){
        if (event.getDamager() instanceof Player){
            if (event.getEntity() instanceof Player){
                if (sumoPlayers.contains(event.getDamager()) && sumoPlayers.contains(event.getEntity())){
                    ((Player) event.getEntity()).setHealth(20);
                    event.setDamage(0);
                    Player damager = (Player) event.getDamager();
                    DataPlayer dataPlayer = Data.getPlayer((Player) event.getEntity());
                    dataPlayer.getSumo().setLastDamager(damager.getUniqueId());
                }
            }
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event){
        Player player = event.getPlayer();
        if (sumoPlayers.contains(player)){
            sumoPlayers.remove(player);
        }
    }

}
