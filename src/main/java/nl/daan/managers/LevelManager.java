package nl.daan.managers;

import nl.daan.data.Data;
import nl.daan.data.objects.player.DataPlayer;
import org.bukkit.entity.Player;

public class LevelManager {

    public static void calculateLevelFromExp(Player player){
        DataPlayer dataPlayer = Data.getPlayer(player);
        int exp = dataPlayer.getExp();
        double neededExpForLevel = (dataPlayer.getLevel() * 100) * 1.02;
        while (exp >= neededExpForLevel){
            exp -= neededExpForLevel;
            dataPlayer.setExp(exp);
            dataPlayer.setLevel(dataPlayer.getLevel() + 1);
            neededExpForLevel = (dataPlayer.getLevel() * 100) * 1.02;
        }
    }

}
