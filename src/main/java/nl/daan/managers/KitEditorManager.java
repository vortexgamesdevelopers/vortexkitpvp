package nl.daan.managers;

import nl.daan.KitPvP;
import nl.daan.data.Data;
import nl.daan.data.objects.player.DataPlayer;
import nl.daan.data.objects.kit.Kit;
import nl.daan.data.objects.kit.KitItem;
import nl.daan.data.objects.kit.ModifiedKit;
import nl.daan.listeners.InventoryClickListener;
import nl.daan.types.ArmorSlot;
import nl.daan.utils.LocationSerializer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.UUID;

import static nl.daan.KitPvP.color;

public class KitEditorManager {

    public UUID uuid;
    public Kit kit;

    public KitEditorManager(Player player, Kit kit) {
        this.uuid = player.getUniqueId();
        this.kit = kit;
    }

    public void init(){
        Player player = Bukkit.getPlayer(this.uuid);
        player.teleport(LocationSerializer.deSerialize(KitPvP.getInstance().getConfig().getString("kitEditor")));
        InventoryClickListener.appendKit(player, this.kit);
        player.sendMessage(color("&aKlik op de sign om je kit op te slaan."));
    }

    public void reset(){
        Player player = Bukkit.getPlayer(this.uuid);
        InventoryClickListener.setItems(player, this.kit.getItems(), this.kit.getArmor());
    }

    public void save(){
        Player player = Bukkit.getPlayer(uuid);
        DataPlayer dataPlayer = Data.getPlayer(player);
        HashMap<Integer, KitItem> items = new HashMap<>();
        ItemStack[] rawArmor = player.getInventory().getArmorContents();
        for (int i = 0; i < player.getInventory().getContents().length; i++){
            items.put(i, new KitItem(player.getInventory().getContents()[i]));
        }
        HashMap<ArmorSlot, KitItem> armor = new HashMap<>();
        armor.put(ArmorSlot.HELMET, new KitItem(rawArmor[3]));
        armor.put(ArmorSlot.CHESTPLATE, new KitItem(rawArmor[2]));
        armor.put(ArmorSlot.LEGGING, new KitItem(rawArmor[1]));
        armor.put(ArmorSlot.BOOTS, new KitItem(rawArmor[0]));
        ModifiedKit modifiedKit = new ModifiedKit(this.kit.getUuid(), items, armor);
        if (!dataPlayer.kits.containsKey(kit.getUuid())){
            dataPlayer.kits.put(kit.getUuid(), modifiedKit);
        } else {
            dataPlayer.kits.replace(kit.getUuid(), modifiedKit);
        }
        player.sendMessage(color("&aDe kit op geslagen."));
        player.teleport(LocationSerializer.deSerialize(KitPvP.getInstance().getConfig().getString("spawn")));
        InventoryManager.resetInventory(player);
    }



}
