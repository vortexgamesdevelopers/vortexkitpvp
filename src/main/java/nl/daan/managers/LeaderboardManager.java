package nl.daan.managers;

import nl.daan.data.Data;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class LeaderboardManager {

    public static void sendTopKills(Player player){
        Data.players.stream()
                .sorted((a1, a2) -> {
                    int kills1 = a1.getKills();
                    int kills2 = a2.getKills();
                    return kills2 - kills1;
                }).limit(25).forEach(f -> {
                    int kills = f.getKills();
                    String playerName = Bukkit.getOfflinePlayer(f.getUuid()).getName();
                    player.sendMessage(playerName + " - " + kills);
                });
    }

}
