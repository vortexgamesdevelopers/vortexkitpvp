package nl.daan.managers;

import nl.daan.data.Data;
import nl.daan.utils.InventoryBuilder;
import nl.daan.utils.ItemstackFactory;
import nl.daan.utils.SkullFactory;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

import static nl.daan.KitPvP.color;

public class InventoryManager {

    public static void kitSelector(Player player){
        InventoryBuilder inventory = new InventoryBuilder(3, "&aSelecteer je kit.");
        if (Data.kits.size() > 0){
            Data.kits.stream().forEach(kit -> {
                ItemStack itemStack = kit.getGuiItem().getItemStack();
                ItemMeta itemMeta = itemStack.getItemMeta();
                itemMeta.setDisplayName(color("&f" + kit.getDisplayName()));
                List<String> lore;
                if (itemMeta.getLore()==null) {
                    lore = new ArrayList<>();
                } else {
                    lore = itemMeta.getLore();
                }
                lore.add(" ");
                for (String line : kit.getKitDescription()){
                    lore.add(color("&f" + line));
                }
                itemMeta.setLore(lore);
                itemStack.setItemMeta(itemMeta);
                inventory.addItem(itemStack);
            });
        } else {
            inventory.addItem(new ItemstackFactory(Material.BARRIER).setDisplayName("&cGeen kits gevonden"));
        }
        player.openInventory(inventory.build());
    }

    public static void resetInventory(Player player){
        player.getInventory().clear();
        player.getInventory().setArmorContents(null);
        InventoryBuilder inventory = new InventoryBuilder(player.getInventory());
        inventory.setItem(0, new ItemstackFactory(Material.COMPASS).setDisplayName("&aSelecteer een kit"));
        inventory.setItem(2, new ItemstackFactory(Material.EMERALD).setDisplayName("&aShop"));
        inventory.setItem(4, new SkullFactory().setOwner(player.getName()).setDisplayName("&aStatistics"));
        inventory.setItem(6, new ItemstackFactory(Material.STICK).setDisplayName("&aKnockbackFFA"));
        inventory.setItem(8, new ItemstackFactory(Material.CHEST).setDisplayName("&aKit Editor"));
        //inventory.setItem(8, new ItemstackFactory(Material.BARRIER).setDisplayName("&cSpectate"));
        player.getInventory().setContents(inventory.build().getContents());
    }

    public static void setSpectatorInventory(Player player){
        InventoryBuilder inventory = new InventoryBuilder(player.getInventory());
        inventory.setItem(8, new ItemstackFactory(Material.BED).setDisplayName("&cStop spectaten"));
        player.getInventory().setContents(inventory.build().getContents());
    }

}
