package nl.daan.utils;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import org.bukkit.Material;
import org.bukkit.inventory.meta.SkullMeta;

import java.lang.reflect.Field;
import java.util.Base64;
import java.util.UUID;

public class SkullFactory extends ItemstackFactory {

    public SkullFactory(){
        super(Material.SKULL_ITEM, 1, (byte) 3);
    }

    public SkullFactory(int amount){
        super(Material.SKULL_ITEM, amount, (byte) 3);
    }

    public SkullFactory setOwner(String owner){
        SkullMeta skullMeta = (SkullMeta) this.getItemMeta();
        skullMeta.setOwner(owner);
        this.setItemMeta(skullMeta);
        return this;
    }

    /*public SkullBuilder setOwner(OfflinePlayer owner){
        SkullMeta skullMeta = (SkullMeta) this.getItemMeta();
        skullMeta.setOwningPlayer(owner);
        this.setItemMeta(skullMeta);
        return this;
    }*/

    public SkullFactory setSkullSkin(String skinUrl){
        SkullMeta skullMeta = (SkullMeta) this.getItemMeta();
        GameProfile localGameProfile = new GameProfile(UUID.randomUUID(), null);
        byte[] arrayOfByte = Base64.getEncoder().encode(String.format("{textures:{SKIN:{url:\"%s\"}}}", new Object[] { skinUrl }).getBytes());
        localGameProfile.getProperties().put("textures", new Property("textures", new String(arrayOfByte)));
        Field localField;
        try {
            localField = skullMeta.getClass().getDeclaredField("profile");
            localField.setAccessible(true);
            localField.set(skullMeta, localGameProfile);
        } catch (NoSuchFieldException|IllegalArgumentException|IllegalAccessException exception) {
            exception.printStackTrace();
        }
        this.setItemMeta(skullMeta);
        return this;
    }

}

