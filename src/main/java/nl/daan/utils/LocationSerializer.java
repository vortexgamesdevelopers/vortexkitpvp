package nl.daan.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class LocationSerializer {

    public static String serialize(Location location){
        String text = "";
        text+= location.getWorld().getName() + ";";
        text+= location.getBlockX() + ";";
        text+= location.getBlockY() + ";";
        text+= location.getBlockZ() + ";";
        text+= location.getYaw() + ";";
        text+= location.getPitch() + ";";
        return text;
    }

    public static Location deSerialize(String locationRaw){
        locationRaw = locationRaw.replaceAll("_", ".");
        String[] splittedLocationRaw = locationRaw.split(";");
        return new Location(
                Bukkit.getWorld(splittedLocationRaw[0]),
                Integer.valueOf(splittedLocationRaw[1]),
                Integer.valueOf(splittedLocationRaw[2]),
                Integer.valueOf(splittedLocationRaw[3]),
                Float.valueOf(splittedLocationRaw[4]),
                Float.valueOf(splittedLocationRaw[5])
        );
    }

}
