package nl.daan.utils;

import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;

import static nl.daan.KitPvP.color;

public class InventoryBuilder {

    private Inventory inventory;

    public Inventory build(){
        return this.inventory;
    }

    public InventoryBuilder(int rows){
        this.inventory = Bukkit.createInventory(null, rows * 9);
    }

    public InventoryBuilder(int rows, String name){
        this.inventory = Bukkit.createInventory(null, rows * 9, color(name));
    }

    public InventoryBuilder(Inventory inventory){
        this.inventory = inventory;
    }

    public InventoryBuilder removeItem(ItemStack itemStack){
        this.inventory.removeItem(itemStack);
        return this;
    }

    public InventoryBuilder addItem(ItemStack itemStack) {
        this.inventory.addItem(itemStack);
        return this;
    }

    public InventoryBuilder setItem(int slot, ItemStack itemStack){
        this.inventory.setItem(slot, itemStack);
        return this;
    }

    public InventoryBuilder setContent(ItemStack[] content){
        this.inventory.setContents(content);
        return this;
    }

    public InventoryBuilder setItems(ItemStack itemStack, int... slots){
        Arrays.stream(slots).forEach(slot -> this.inventory.setItem(slot, itemStack));
        return this;
    }

}
