package nl.daan.types;

public enum Suffix {

    NONE(""),
    DERP("&dDerp"),
    NOOB("&fNoob"),
    PRO("&4Pro");

    public String prefix;

    Suffix(String prefix){
        this.prefix = prefix;
    }

    public String getPrefix() {
        return prefix;
    }
}
