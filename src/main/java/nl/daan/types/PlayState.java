package nl.daan.types;

public enum PlayState {

    IDLE(false),
    PLAYING(true),
    SPECTATING(false);

    private boolean pvpEnabled;

    PlayState(boolean pvpEnabled){
        this.pvpEnabled = pvpEnabled;
    }

    public boolean isPvpEnabled() {
        return pvpEnabled;
    }
}
