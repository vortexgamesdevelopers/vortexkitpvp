package nl.daan.types;

public enum ClanPermission {
    INVITE,
    KICK,
    DEINVITE,
    CHANGEDESCRIPTION,
    CHANGENAME,
    TOGGLECHAT
}
