package nl.daan.types;

public enum Rank {

    EIGENAAR(1,"&4Eigenaar", "&1&4EIG"),
    DEVELOPER(2,"&cDeveloper", "&2&cDEV"),
    PL(3,"&dPL", "&3&cPL"),
    ADMIN(4,"&4Admin", "&4ADM"),
    MODERATOR(5,"&2Mod", "&5&2MOD"),
    HELPER(6,"&aHelper", "&6&aHEL"),
    YOUTUBE(7,"&cYou&ftuber", "&cY&fT"),
    SPELER(8,"&7Speler", "&7&7SPE");

    public String prefix, tabPrefix;
    public int Id;

    Rank(int id, String prefix, String tabPrefix){
        this.prefix = prefix;
        this.tabPrefix = tabPrefix;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getTabPrefix() {
        return tabPrefix;
    }

    public int getId() {
        return Id;
    }
}
