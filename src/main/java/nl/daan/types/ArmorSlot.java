package nl.daan.types;

public enum ArmorSlot {

    HELMET,
    CHESTPLATE,
    LEGGING,
    BOOTS

}
