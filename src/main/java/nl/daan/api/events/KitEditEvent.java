package nl.daan.api.events;

import nl.daan.data.Data;
import nl.daan.data.objects.player.DataPlayer;
import nl.daan.data.objects.kit.Kit;
import nl.daan.data.objects.kit.KitItem;
import nl.daan.types.ArmorSlot;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

import static nl.daan.KitPvP.color;

public class KitEditEvent extends Event implements Cancellable {

    private Player player;
    private boolean cancelled;
    private Kit kit;
    private String kitName;

    public void execute(){
        if (!this.cancelled){
            setItems(player, kit);
            player.sendMessage(color("&aItems van \"" + kitName + "\" aangepast."));
            for (Player player : Bukkit.getOnlinePlayers()){
                DataPlayer dataPlayer = Data.getPlayer(player);
                if (dataPlayer.getKits().containsKey(this.kit.getUuid())){
                    dataPlayer.getKits().remove(this.kit.getUuid());
                }
            }
        }
    }

    public KitEditEvent(Player player, String  kitName){
        this.player = player;
        this.kitName = kitName;
        this.kit = Data.getKit(kitName);
    }

    public Player getPlayer() {
        return player;
    }

    private static final HandlerList handlers = new HandlerList();

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public Kit getKit() {
        return kit;
    }

    public void setKit(Kit kit) {
        this.kit = kit;
    }

    public String getKitName() {
        return kitName;
    }

    public void setKitName(String kitName) {
        this.kitName = kitName;
    }

    public static void setItems(Player player, Kit kit){
        kit.armor.clear();
        kit.items.clear();
        ItemStack[] items = player.getInventory().getContents();
        ItemStack[] armor = player.getInventory().getArmorContents();
        for (int i = 0; i < items.length; i++){
            kit.items.put(i, new KitItem(items[i]));
        }
        kit.armor.put(ArmorSlot.HELMET, new KitItem(armor[3]));
        kit.armor.put(ArmorSlot.CHESTPLATE, new KitItem(armor[2]));
        kit.armor.put(ArmorSlot.LEGGING, new KitItem(armor[1]));
        kit.armor.put(ArmorSlot.BOOTS, new KitItem(armor[0]));
    }

}
