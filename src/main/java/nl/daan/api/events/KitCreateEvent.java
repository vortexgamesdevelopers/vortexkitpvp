package nl.daan.api.events;

import nl.daan.data.Data;
import nl.daan.data.objects.kit.Kit;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import static nl.daan.KitPvP.color;

public class KitCreateEvent extends Event implements Cancellable {

    private Kit kit;
    private Player player;
    private String name;
    private boolean cancelled;

    private static final HandlerList handlers = new HandlerList();

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public void execute(){
        if (!this.cancelled){
            Kit kit = new Kit(this.name, this.player.getInventory().getContents(), this.player.getInventory().getArmorContents());
            kit.setDisplayName(color(this.name));
            player.sendMessage(color("&aKit \"" + this.name + "\" gemaakt."));
            Data.kits.add(kit);
        }
    }

    public KitCreateEvent(Player player, String name){
        this.player = player;
        this.name = name;
        this.kit = new Kit(name, player.getInventory().getContents(), player.getInventory().getArmorContents());
    }

    public Kit getKit() {
        return kit;
    }

    public void setKit(Kit kit) {
        this.kit = kit;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }
}
