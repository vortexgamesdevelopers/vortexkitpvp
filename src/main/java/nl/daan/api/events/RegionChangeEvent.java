package nl.daan.api.events;

import nl.daan.data.Data;
import nl.daan.data.objects.player.DataPlayer;
import nl.daan.types.PlayState;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class RegionChangeEvent extends Event {

    private Player player;
    private Location to, from;
    private Block bottomTo, bottomFrom;
    private Material materialTo, materialFrom;

    private static final HandlerList handlers = new HandlerList();

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public void execute(){
        DataPlayer dataPlayer = Data.getPlayer(player);
        if (dataPlayer.getPlayState().equals(PlayState.SPECTATING)) return;
        if (this.materialFrom.equals(Material.DIAMOND_BLOCK) && this.materialTo.equals(Material.BEDROCK)){
            dataPlayer.setPlayState(PlayState.PLAYING);
            /*if (Data.getPlayer(player).getLastUsedKit() != null){
                InventoryClickListener.appendKit(player, Data.kits.stream().filter(i -> i.getUuid().equals(Data.getPlayer(player).getLastUsedKit())).findFirst().get());
            }*/
        }
        if (this.materialFrom.equals(Material.BEDROCK) && this.materialTo.equals(Material.DIAMOND_BLOCK)){
            dataPlayer.setPlayState(PlayState.IDLE);
        }
    }

    public RegionChangeEvent(Player player, Location to, Location from) {
        this.player = player;
        this.to = to;
        this.from = from;
        this.bottomTo = Bukkit.getServer().getWorld(to.getWorld().getName()).getBlockAt(to.getBlockX(), 0, to.getBlockZ());
        this.bottomFrom = Bukkit.getServer().getWorld(from.getWorld().getName()).getBlockAt(from.getBlockX(), 0, from.getBlockZ());
        this.materialTo = this.bottomTo.getType();
        this.materialFrom = this.bottomFrom.getType();
    }

    public Player getPlayer() {
        return player;
    }

    public Location getTo() {
        return to;
    }

    public Location getFrom() {
        return from;
    }


    public Block getBottomTo() {
        return bottomTo;
    }

    public Block getBottomFrom() {
        return bottomFrom;
    }

    public Material getMaterialTo() {
        return materialTo;
    }

    public Material getMaterialFrom() {
        return materialFrom;
    }
}
