package nl.daan.api.events;

import nl.daan.data.Data;
import nl.daan.data.objects.kit.Kit;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

import static nl.daan.KitPvP.color;

public class KitChangePermissionEvent extends Event implements Listener {

    private Player player;
    private String kitName, permission;
    private Kit kit;
    private boolean cancelled;

    public void execute(){
        Kit kit = Data.getKit(kitName);
        if (this.permission.equalsIgnoreCase("unset")) {
            kit.setPermission(null);
            player.sendMessage(color("&aPermission van de kit \"" + kitName + "\n verwijderd."));
        } else {
            kit.setPermission(this.permission.toLowerCase());
            player.sendMessage(color("&aPermission van de kit \"" + kitName + "\" gezet naar \"" + this.permission.toLowerCase() + "\""));
        }
    }

    public KitChangePermissionEvent(Player player, String kitName, String permssion){
        this.player = player;
        this.kitName = kitName;
        this.permission = permssion;
        this.kit = Data.getKit(kitName);
    }

    private static final HandlerList handlers = new HandlerList();

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public String getKitName() {
        return kitName;
    }

    public void setKitName(String kitName) {
        this.kitName = kitName;
    }

    public Kit getKit() {
        return kit;
    }

    public void setKit(Kit kit) {
        this.kit = kit;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }
}
