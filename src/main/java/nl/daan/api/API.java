package nl.daan.api;

import nl.daan.KitPvP;
import org.bukkit.plugin.Plugin;

import java.io.File;

public class API {

    public static void setupDirectorys(){
        Plugin plugin = KitPvP.getInstance();
        createDirectory(plugin.getDataFolder());
        createDirectory(new File(plugin.getDataFolder() + File.separator + "Backup"));
        createDirectory(new File(plugin.getDataFolder() + File.separator + "Data"));
        createDirectory(new File(plugin.getDataFolder() + File.separator + "Data" + File.separator + "Players"));
        createDirectory(new File(plugin.getDataFolder() + File.separator + "Data" + File.separator + "Kits"));
    }

    public static void setupFiles(){

    }

    private static boolean fileExists(File file){
        return file.exists();
    }

    private static boolean fileExists(String name){
        Plugin plugin = KitPvP.getInstance();
        File file = new File(plugin.getDataFolder() + File.separator + name);
        return file.exists();
    }

    private static void createDirectory(File file){
        if (!file.exists()){
            try {
                file.mkdir();
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

}
