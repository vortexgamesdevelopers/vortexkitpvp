package nl.daan;

import nl.daan.api.API;
import nl.daan.commands.*;
import nl.daan.data.Data;
import nl.daan.data.objects.player.DataPlayer;
import nl.daan.listeners.*;
import nl.daan.managers.SumoManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;

public class KitPvP extends JavaPlugin {

    private static KitPvP instance;
    public static boolean DEV = true;
    private static Scoreboards scoreboards;

    @Override
    public void onEnable(){
        instance = this;
        API.setupDirectorys();
        API.setupFiles();
        Data.load();
        Backup backup = new Backup();
        backup.makeBackup();
        registerListeners(
                new JoinListener(),
                new DamageListener(),
                new RespawnListener(),
                new KillListener(),
                new BlockListener(),
                new InteractListener(),
                new InventoryClickListener(),
                new MoveListener(),
                new ChatListener(),
                new SpawnCommand(),
                new FoodChangeListener(),
                new EntityDeathListener(),
                new QuitListener(),
                new DropListener(),
                new SumoManager(),
                new SignChangeListener(),
                new SignClickListener(),
                new WeatherListener(),
                new commandListener()
        );
        registerCommands(
                new KitCommand(),
                new SpawnCommand(),
                new RankCommand(),
                new UnbreakableCommand(),
                new SetSpawnCommand(),
                new playervplayerstatsCommand(),
                new SuffixCommand(),
                new ClanCommand(),
                new BackupCommand()
        );
        Bukkit.getOnlinePlayers().stream().forEach(player -> {
            if (!Data.players.stream().anyMatch(i -> i.getUuid().equals(player.getUniqueId()))){
                DataPlayer dataPlayer = new DataPlayer(player.getUniqueId());
                Data.players.add(dataPlayer);
            }
        });
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                Bukkit.getServer().getOnlinePlayers().stream().forEach(player -> {
                    DataPlayer dataPlayer = Data.getPlayer(player);
                    dataPlayer.getPlayTime().append(player);
                });
            }
        }, 20L, 20L);
        scoreboards = new Scoreboards();
    }

    @Override
    public void onDisable(){
        for (Player player : SumoManager.sumoPlayers){
            SumoManager.leave(player);
        }
        Data.save();
    }

    private void registerListeners(Listener... listeners){
        Arrays.stream(listeners).forEach(listener -> Bukkit.getPluginManager().registerEvents(listener, this));
    }

    private void registerCommands(CommandBase... commands){
        Arrays.stream(commands).forEach(command -> {
            getCommand(command.getCommand()).setExecutor(command);
            getCommand(command.getCommand()).setTabCompleter(command);
        });
    }

    public static KitPvP getInstance(){
        return instance;
    }

    public static String color(String text){
        return ChatColor.translateAlternateColorCodes('&', text);
    }

    public static Scoreboards getScoreboards() {
        return scoreboards;
    }
}
