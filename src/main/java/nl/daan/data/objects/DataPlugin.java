package nl.daan.data.objects;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class DataPlugin {

    public Location spawn;

    public DataPlugin(){
        this.spawn = Bukkit.getServer().getWorlds().get(0).getSpawnLocation();
    }

    public Location getSpawn() {
        return spawn;
    }

    public void setSpawn(Location spawn) {
        this.spawn = spawn;
    }
}
