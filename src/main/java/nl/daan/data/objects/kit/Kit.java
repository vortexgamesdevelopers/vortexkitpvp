package nl.daan.data.objects.kit;

import nl.daan.types.ArmorSlot;
import nl.daan.utils.ItemstackFactory;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public class Kit implements Cloneable {

    public UUID uuid;
    public String name, permission, displayName;
    public KitItem guiItem;
    public HashMap<Integer, KitItem> items = new HashMap<>();
    public HashMap<ArmorSlot, KitItem> armor = new HashMap<>();
    public List<String> kitDescription;

    public Kit(String name, ItemStack[] items, ItemStack[] armor){
        this.name = name;
        this.permission = "";
        this.uuid = UUID.randomUUID();
        this.displayName = name;
        for (int i = 0; i < items.length; i++){
            this.items.put(i, new KitItem(items[i]));
        }
        if (armor[3] != null) this.armor.put(ArmorSlot.HELMET, new KitItem(armor[3]));
        if (armor[2] != null) this.armor.put(ArmorSlot.CHESTPLATE, new KitItem(armor[2]));
        if (armor[1] != null) this.armor.put(ArmorSlot.LEGGING, new KitItem(armor[1]));
        if (armor[0] != null) this.armor.put(ArmorSlot.BOOTS, new KitItem(armor[0]));
        this.kitDescription = new ArrayList<>();
        this.guiItem = new KitItem(new ItemstackFactory(Material.BARRIER));
    }

    public Kit(Kit kit){
        this.name = kit.getName();
        this.permission = kit.getPermission();
        this.uuid = kit.getUuid();
        this.displayName = this.getDisplayName();
        this.items = kit.getItems();
        this.armor = kit.getArmor();
        this.kitDescription = kit.getKitDescription();
        this.guiItem = kit.getGuiItem();
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public HashMap<ArmorSlot, KitItem> getArmor() {
        return armor;
    }

    public void setArmor(HashMap<ArmorSlot, KitItem> armor) {
        this.armor = armor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public KitItem getGuiItem() {
        return guiItem;
    }

    public void setGuiItem(KitItem guiItem) {
        this.guiItem = guiItem;
    }

    public HashMap<Integer, KitItem> getItems() {
        return items;
    }

    public void setItems(HashMap<Integer, KitItem> items) {
        this.items = items;
    }

    public List<String> getKitDescription() {
        return kitDescription;
    }

    public void setKitDescription(List<String> kitDescription) {
        this.kitDescription = kitDescription;
    }

    public Object clone() throws CloneNotSupportedException {
        return this.clone();
    }

}
