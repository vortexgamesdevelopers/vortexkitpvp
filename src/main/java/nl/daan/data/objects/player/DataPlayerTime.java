package nl.daan.data.objects.player;

import nl.daan.api.events.*;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class DataPlayerTime {

    private int seconds, minutes, hours, days;

    public DataPlayerTime(){
        this.seconds = 0;
        this.minutes = 0;
        this.hours = 0;
        this.days = 0;
    }

    public void append(Player player){
        seconds++;
        if (seconds == 60){
            seconds = 0;
            minutes++;
            ReachMinuteEvent event = new ReachMinuteEvent(player, this.minutes);
            Bukkit.getPluginManager().callEvent(event);
        }
        if (minutes == 60){
            minutes = 0;
            hours++;
            ReachHourEvent event = new ReachHourEvent(player, this.hours);
            Bukkit.getPluginManager().callEvent(event);
        }
        if (hours == 24){
            hours = 0;
            days++;
            ReachDayEvent event = new ReachDayEvent(player, this.days);
            Bukkit.getPluginManager().callEvent(event);
        }
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }
}
