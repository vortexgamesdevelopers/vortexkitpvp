package nl.daan.data.objects.player;

import nl.daan.data.objects.kit.KitItem;
import nl.daan.data.objects.kit.ModifiedKit;
import nl.daan.managers.KitEditorManager;
import nl.daan.managers.SpectatorManager;
import nl.daan.types.PlayState;
import nl.daan.types.Rank;
import nl.daan.types.Suffix;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class DataPlayer {

    public UUID uuid, lastUsedKit, clan;
    public int kills, deaths, killstreak, bowShots, bowShotHits, chatMessagesSend, blockMoved, joins, leftClicks, rightClicks, level, prestige, exp;
    public double damageDealt, damageTaken, gold;
    public DataPlayerTime playTime;
    public PlayState playState;
    public Rank rank;
    public HashMap<UUID, Integer> playerKills, playerDeaths;
    public List<KitItem> purchasesItems;
    public boolean enderchest;
    public DataPlayerSumo dataPlayerSumo;
    public HashMap<UUID, ModifiedKit> kits;
    public KitEditorManager kitEditorManager;
    public SpectatorManager spectatorManager;
    public Suffix suffix;
    public List<Suffix> availableSuffix;
    public List<UUID> clanInvites;

    public DataPlayer(UUID uuid){
        this.uuid = uuid;
        this.kills = 0;
        this.deaths = 0;
        this.killstreak = 0;
        this.bowShots = 0;
        this.bowShotHits = 0;
        this.chatMessagesSend = 0;
        this.blockMoved = 0;
        this.joins = 0;
        this.leftClicks = 0;
        this.rightClicks = 0;
        this.damageDealt = 0;
        this.damageTaken = 0;
        this.playTime = new DataPlayerTime();
        this.playState = PlayState.IDLE;
        this.rank = Rank.SPELER;
        this.level = 1;
        this.prestige = 0;
        this.exp = 0;
        this.gold = 0.0;
        this.playerKills = new HashMap<>();
        this.playerDeaths = new HashMap<>();
        this.purchasesItems = new ArrayList<>();
        this.enderchest = false;
        this.dataPlayerSumo = new DataPlayerSumo();
        this.kits = new HashMap<>();
        this.suffix = Suffix.NONE;
        this.availableSuffix = new ArrayList<>();
        this.clanInvites = new ArrayList<>();
    }

    public List<UUID> getClanInvites() {
        if (this.clanInvites == null) return new ArrayList<>();
        return clanInvites;
    }

    public void setClanInvites(List<UUID> clanInvites) {
        this.clanInvites = clanInvites;
    }

    public UUID getClan() {
        if (this.clan == null) return null;
        return clan;
    }

    public void setClan(UUID clan) {
        this.clan = clan;
    }

    public Suffix getSuffix() {
        if (this.suffix == null) return Suffix.NONE;
        return suffix;
    }

    public void setSuffix(Suffix suffix) {
        this.suffix = suffix;
    }

    public List<Suffix> getAvailableSuffix() {
        if (this.availableSuffix == null) return new ArrayList<>();
        return availableSuffix;
    }

    public void addSuffix(Suffix suffix){
        List<Suffix> suffixes = this.getAvailableSuffix();
        suffixes.add(suffix);
        this.setAvailableSuffix(suffixes);
    }

    public void removeSuffix(Suffix suffix){
        List<Suffix> suffixes = this.getAvailableSuffix();
        suffixes.remove(suffix);
        this.setAvailableSuffix(suffixes);
    }

    public void setAvailableSuffix(List<Suffix> availableSuffix) {
        this.availableSuffix = availableSuffix;
    }

    public SpectatorManager getSpectatorManager() {
        return spectatorManager;
    }

    public void setSpectatorManager(SpectatorManager spectatorManager) {
        this.spectatorManager = spectatorManager;
    }

    public KitEditorManager getKitEditorManager() {
        return kitEditorManager;
    }

    public void setKitEditorManager(KitEditorManager kitEditorManager) {
        this.kitEditorManager = kitEditorManager;
    }

    public DataPlayerSumo getDataPlayerSumo() {
        return dataPlayerSumo;
    }

    public HashMap<UUID, ModifiedKit> getKits() {
        if (this.kits == null) this.kits = new HashMap<>();
        return kits;
    }

    public void setKits(HashMap<UUID, ModifiedKit> kits) {
        this.kits = kits;
    }

    public DataPlayerSumo getSumo() {
        if (this.dataPlayerSumo == null){
            this.dataPlayerSumo = new DataPlayerSumo();
            return this.dataPlayerSumo;
        } else {
            return dataPlayerSumo;
        }
    }

    public void setDataPlayerSumo(DataPlayerSumo dataPlayerSumo) {
        this.dataPlayerSumo = dataPlayerSumo;
    }

    public boolean isEnderchest() {
        return enderchest;
    }

    public void setEnderchest(boolean enderchest) {
        this.enderchest = enderchest;
    }

    public List<KitItem> getPurchasesItems() {
        if (this.purchasesItems == null) return new ArrayList<>();
        return purchasesItems;
    }

    public void setPurchasesItems(List<KitItem> purchasesItems) {
        if (this.purchasesItems == null) this.purchasesItems = new ArrayList<>();
        this.purchasesItems = purchasesItems;
    }

    public void addPurchasedItem(KitItem kitItem){
        List<KitItem> kitItems = this.getPurchasesItems();
        kitItems.add(kitItem);
        this.setPurchasesItems(kitItems);
    }

    public void clearPurchasedItems(){
        this.purchasesItems.clear();
    }

    public UUID getLastUsedKit() {
        return lastUsedKit;
    }

    public void setLastUsedKit(UUID lastUsedKit) {
        this.lastUsedKit = lastUsedKit;
    }

    public HashMap<UUID, Integer> getPlayerKills() {
        if (this.playerKills == null) this.playerKills = new HashMap<>();
        return playerKills;
    }

    public void appendKill(Player player){
        if(this.playerKills == null) this.playerKills = new HashMap<>();
        if (!this.getPlayerKills().containsKey(player.getUniqueId())){
            this.getPlayerKills().put(player.getUniqueId(), 1);
        } else {
            this.getPlayerKills().replace(player.getUniqueId(), this.getPlayerKills().get(player.getUniqueId()) + 1);
        }
    }

    public void appendDeaths(Player player){
        if(this.playerDeaths == null) this.playerDeaths = new HashMap<>();
        if (!this.getPlayerDeaths().containsKey(player.getUniqueId())){
            this.getPlayerDeaths().put(player.getUniqueId(), 1);
        } else {
            this.getPlayerDeaths().replace(player.getUniqueId(), this.getPlayerDeaths().get(player.getUniqueId()) + 1);
        }
    }

    public void setPlayerKills(HashMap<UUID, Integer> playerKills) {
        this.playerKills = playerKills;
    }

    public HashMap<UUID, Integer> getPlayerDeaths() {
        if (this.playerDeaths == null) this.playerDeaths = new HashMap<>();
        return playerDeaths;
    }

    public void setPlayerDeaths(HashMap<UUID, Integer> playerDeaths) {
        this.playerDeaths = playerDeaths;
    }

    public PlayState getPlayState() {
        return playState;
    }

    public void setPlayState(PlayState playState) {
        this.playState = playState;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public int getKills() {
        return kills;
    }

    public void setKills(int kills) {
        this.kills = kills;
    }

    public int getDeaths() {
        return deaths;
    }

    public void setDeaths(int deaths) {
        this.deaths = deaths;
    }

    public int getKillstreak() {
        return killstreak;
    }

    public void setKillstreak(int killstreak) {
        this.killstreak = killstreak;
    }

    public DataPlayerTime getPlayTime() {
        return playTime;
    }

    public void setPlayTime(DataPlayerTime playTime) {
        this.playTime = playTime;
    }

    public int getBowShots() {
        return bowShots;
    }

    public void setBowShots(int bowShots) {
        this.bowShots = bowShots;
    }

    public int getBowShotHits() {
        return bowShotHits;
    }

    public void setBowShotHits(int bowShotHits) {
        this.bowShotHits = bowShotHits;
    }

    public int getChatMessagesSend() {
        return chatMessagesSend;
    }

    public void setChatMessagesSend(int chatMessagesSend) {
        this.chatMessagesSend = chatMessagesSend;
    }

    public int getBlockMoved() {
        return blockMoved;
    }

    public void setBlockMoved(int blockMoved) {
        this.blockMoved = blockMoved;
    }

    public int getJoins() {
        return joins;
    }

    public void setJoins(int joins) {
        this.joins = joins;
    }

    public int getLeftClicks() {
        return leftClicks;
    }

    public void setLeftClicks(int leftClicks) {
        this.leftClicks = leftClicks;
    }

    public int getRightClicks() {
        return rightClicks;
    }

    public void setRightClicks(int rightClicks) {
        this.rightClicks = rightClicks;
    }

    public double getDamageDealt() {
        return damageDealt;
    }

    public void setDamageDealt(double damageDealt) {
        this.damageDealt = damageDealt;
    }

    public double getDamageTaken() {
        return damageTaken;
    }

    public void setDamageTaken(double damageTaken) {
        this.damageTaken = damageTaken;
    }

    public Rank getRank() {
        return rank;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getPrestige() {
        return prestige;
    }

    public void setPrestige(int prestige) {
        this.prestige = prestige;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public double getGold() {
        return gold;
    }

    public void setGold(double gold) {
        this.gold = gold;
    }
}
