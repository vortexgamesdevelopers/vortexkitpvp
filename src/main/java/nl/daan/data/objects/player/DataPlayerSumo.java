package nl.daan.data.objects.player;

import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

public class DataPlayerSumo {

    public int kills, deaths;
    public HashMap<UUID, Integer> playerKills, playerDeaths;
    public UUID lastDamager;

    public DataPlayerSumo(){
        this.kills = 0;
        this.deaths = 0;
        this.playerKills = new HashMap<>();
        this.playerDeaths = new HashMap<>();
    }

    public void appendKill(Player player){
        if (player.getUniqueId() == null) return;
        if (this.playerKills == null) this.playerKills = new HashMap<>();
        if (!this.playerKills.containsKey(player.getUniqueId())) {
            this.playerKills.put(player.getUniqueId(), 1);
        } else {
            this.playerKills.replace(player.getUniqueId(), this.playerKills.get(player.getUniqueId()) + 1);
        }
    }

    public void appendDeath(Player player){
        if (player.getUniqueId() == null) return;
        if (this.playerDeaths == null) this.playerDeaths = new HashMap<>();
        if (!this.playerDeaths.containsKey(player.getUniqueId())) {
            this.playerDeaths.put(player.getUniqueId(), 1);
        } else {
            this.playerDeaths.replace(player.getUniqueId(), this.playerDeaths.get(player.getUniqueId()) + 1);
        }
    }

    public UUID getLastDamager() {
        return lastDamager;
    }

    public void setLastDamager(UUID lastDamager) {
        this.lastDamager = lastDamager;
    }

    public int getKills() {
        return kills;
    }

    public void setKills(int kills) {
        this.kills = kills;
    }

    public int getDeaths() {
        return deaths;
    }

    public void setDeaths(int deaths) {
        this.deaths = deaths;
    }

    public HashMap<UUID, Integer> getPlayerKills() {
        return playerKills;
    }

    public void setPlayerKills(HashMap<UUID, Integer> playerKills) {
        this.playerKills = playerKills;
    }

    public HashMap<UUID, Integer> getPlayerDeaths() {
        return playerDeaths;
    }

    public void setPlayerDeaths(HashMap<UUID, Integer> playerDeaths) {
        this.playerDeaths = playerDeaths;
    }
}
