package nl.daan.data.objects.clan;

import java.util.UUID;

public class ClanPlayer {

    public UUID clanRank;
    public int messages;

    public ClanPlayer(Clan clan){
        this.clanRank = clan.getDefaultRank();
        this.messages = 0;
    }

    public UUID getClanRank() {
        return clanRank;
    }

    public void setClanRank(UUID clanRank) {
        this.clanRank = clanRank;
    }

    public void addMessage(){
        this.setMessages(this.getMessages() + 1);
    }

    public int getMessages() {
        return messages;
    }

    public void setMessages(int messages) {
        this.messages = messages;
    }
}
