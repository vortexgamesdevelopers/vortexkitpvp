package nl.daan.data.objects.clan;

import nl.daan.data.Data;
import nl.daan.data.objects.player.DataPlayer;
import nl.daan.types.ClanPermission;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import static nl.daan.KitPvP.color;

public class Clan {

    public UUID uuid, owner, defaultRank;
    public String name;
    public HashMap<UUID, ClanPlayer> players;
    public List<ClanRank> ranks;
    public boolean open;

    public Clan(String name, UUID owner){
        this.name = name;
        this.owner = owner;
        this.uuid = UUID.randomUUID();
        this.players = new HashMap<>();
        this.ranks = new ArrayList<>();
        ClanRank ownerRank = new ClanRank("Owner");
        for (ClanPermission clanPermission : ClanPermission.values()){
            ownerRank.addPermission(clanPermission);
        }
        ownerRank.setChangeAble(false);
        this.ranks.add(ownerRank);
        ClanRank defaultRank = new ClanRank("Members");
        this.ranks.add(defaultRank);
        this.defaultRank = defaultRank.getUuid();
        this.players.put(owner, new ClanPlayer(this));
        this.players.get(owner).setClanRank(ownerRank.getUuid());
        Player ownerPlayer = Bukkit.getPlayer(owner);
        DataPlayer dataPlayer = Data.getPlayer(ownerPlayer);
        dataPlayer.setClan(this.getUuid());
        this.open = false;
    }

    public void disband(){
        for (UUID clanPlayer : this.players.keySet()){
            OfflinePlayer player = Bukkit.getOfflinePlayer(clanPlayer);
            DataPlayer dataPlayer = Data.getPlayer(player);
            dataPlayer.setClan(null);
            if (player.isOnline()){
                player.getPlayer().sendMessage(color("&cDe clan is verwijderd door de eigenaar."));
            }
        }
        Data.clans.remove(this);
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public UUID getOwner() {
        return owner;
    }

    public void setOwner(UUID owner) {
        this.owner = owner;
    }

    public UUID getDefaultRank() {
        return defaultRank;
    }

    public void setDefaultRank(UUID defaultRank) {
        this.defaultRank = defaultRank;
    }

    public List<ClanRank> getRanks() {
        return ranks;
    }

    public void setRanks(List<ClanRank> ranks) {
        this.ranks = ranks;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HashMap<UUID, ClanPlayer> getPlayers() {
        return players;
    }

    public void setPlayers(HashMap<UUID, ClanPlayer> players) {
        this.players = players;
    }
}
