package nl.daan.data.objects.clan;

import nl.daan.types.ClanPermission;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ClanRank {

    public UUID uuid;
    public String name;
    public List<ClanPermission> permissions;
    public boolean changeAble;

    public ClanRank(String name){
        this.name = name;
        this.uuid = UUID.randomUUID();
        this.permissions = new ArrayList<>();
        this.changeAble = true;
    }

    public boolean isChangeAble() {
        return changeAble;
    }

    public void setChangeAble(boolean changeAble) {
        this.changeAble = changeAble;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addPermission(ClanPermission clanPermission){
        List<ClanPermission> current = this.getPermissions();
        current.add(clanPermission);
        this.setPermissions(current);
    }

    public List<ClanPermission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<ClanPermission> permissions) {
        this.permissions = permissions;
    }
}
