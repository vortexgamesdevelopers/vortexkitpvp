package nl.daan.data;

import com.google.gson.Gson;
import nl.daan.KitPvP;
import nl.daan.data.objects.clan.Clan;
import nl.daan.data.objects.player.DataPlayer;
import nl.daan.data.objects.DataPlugin;
import nl.daan.data.objects.kit.Kit;
import org.apache.commons.io.FileUtils;
import org.bukkit.OfflinePlayer;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class Data {

    public static List<DataPlayer> players = new ArrayList<>();
    public static List<Kit> kits = new ArrayList<>();
    public static List<Clan> clans = new ArrayList<>();
    public static DataPlugin plugin;

    public static DataPlayer getPlayer(OfflinePlayer player){
        return players.stream().filter(i -> i.getUuid().equals(player.getUniqueId())).findFirst().get();
    }

    public static Kit getKit(String kitName){
        return kits.stream().filter(i -> i.getName().equalsIgnoreCase(kitName)).findFirst().get();
    }

    public static Clan getClan(String name){
        return clans.stream().filter(i -> i.getName().equalsIgnoreCase(name)).findFirst().get();
    }

    public static Clan getClan(UUID uuid){
        return clans.stream().filter(i -> i.getUuid().equals(uuid)).findFirst().get();
    }

    public static void load() {
        Plugin plugin = KitPvP.getInstance();
        Gson gson = new Gson();
        File dataFolder = new File(plugin.getDataFolder() + File.separator + "Data");
        if (!dataFolder.exists()){
            try {
                dataFolder.mkdir();
            } catch (Exception e){
                e.printStackTrace();
            }
        }
        File playerDataFolder = getDirectory(dataFolder, "Players");
        File kitDataFolder = getDirectory(dataFolder, "Kits");
        File clanDataFolder = getDirectory(dataFolder, "Clans");
        if (playerDataFolder.listFiles().length > 0){
            Arrays.stream(playerDataFolder.listFiles()).filter(file -> !file.isDirectory()).filter(file -> file.getName().contains(".json")).forEach(file -> {
                try {
                    players.add(gson.fromJson(FileUtils.readFileToString(file), DataPlayer.class));
                } catch (IOException e){
                    e.printStackTrace();
                }
            });
        }
        if (kitDataFolder.listFiles().length > 0){
            Arrays.stream(kitDataFolder.listFiles()).filter(file -> !file.isDirectory()).filter(file -> file.getName().contains(".json")).forEach(file -> {
                try {
                    kits.add(gson.fromJson(FileUtils.readFileToString(file), Kit.class));
                } catch (IOException e){
                    e.printStackTrace();
                }
            });
        }
        if (clanDataFolder.listFiles().length > 0){
            Arrays.stream(clanDataFolder.listFiles()).filter(file -> !file.isDirectory()).filter(file -> file.getName().contains(".json")).forEach(file -> {
                try {
                    clans.add(gson.fromJson(FileUtils.readFileToString(file), Clan.class));
                } catch (IOException e){
                    e.printStackTrace();
                }
            });
        }
    }

    public static void save(){
        Plugin plugin = KitPvP.getInstance();
        Gson gson = new Gson();
        File dataFolder = new File(plugin.getDataFolder() + File.separator + "Data");
        File playerDataFolder = getDirectory(dataFolder, "Players");
        File kitDataFolder = getDirectory(dataFolder, "Kits");
        File clanDataFolder = getDirectory(dataFolder, "Clans");
        clearFolder(playerDataFolder);
        players.stream().forEach(player -> {
            try {
                File file = new File(playerDataFolder + File.separator + player.getUuid() + ".json");
                file.createNewFile();
                FileUtils.write(file, gson.toJson(player));
            } catch (Exception e){
                e.printStackTrace();
            }
        });
        clearFolder(kitDataFolder);
        kits.stream().forEach(kit -> {
            try {
                File file = new File(kitDataFolder + File.separator + kit.getUuid() + ".json");
                file.createNewFile();
                FileUtils.write(file, gson.toJson(kit));
            } catch (IOException e){
                e.printStackTrace();
            }
        });
        clearFolder(clanDataFolder);
        clans.stream().forEach(clan -> {
            try {
                File file = new File(clanDataFolder + File.separator + clan.getUuid() + ".json");
                file.createNewFile();
                FileUtils.write(file, gson.toJson(clan));
            } catch (IOException e){
                e.printStackTrace();
            }
        });
    }

    private static void clearFolder(File file){
        for (File folderFile : file.listFiles()){
            folderFile.delete();
        }
    }

    public static void reload(){
        save();
        load();
    }

    private static File getDirectory(File mainFolder, String directoryName){
        File file = new File(mainFolder + File.separator + directoryName);
        if (!file.exists()){
            try {
                file.mkdir();
            } catch (Exception e){
                e.printStackTrace();
            }
        }
        return new File(mainFolder + File.separator + directoryName);
    }

}
